package frc.examples.talonSrx;

import edu.wpi.first.wpilibj.IterativeRobot;



/**
 * Example demonstrating the Position closed-loop servo.
 * Tested with Logitech F350 USB Gamepad inserted into Driver Station]
 *
 * Be sure to select the correct feedback sensor using SetFeedbackDevice() below.
 *
 * After deploying/debugging this to your RIO, first use the left Y-stick
 * to throttle the Talon manually.  This will confirm your hardware setup.
 * Be sure to confirm that when the Talon is driving forward (green) the
 * position sensor is moving in a positive direction.  If this is not the cause
 * flip the boolean input to the reverseSensor() call below.
 *
 * Once you've ensured your feedback device is in-phase with the motor,
 * use the button shortcuts to servo to target position.
 *
 * Tweak the PID gains accordingly.
 */

//import edu.wpi.first.wpilibj.CANTalon;
//import edu.wpi.first.wpilibj.CANTalon.FeedbackDevice;
//import edu.wpi.first.wpilibj.CANTalon.TalonControlMode;
//import edu.wpi.first.wpilibj.IterativeRobot;
//import edu.wpi.first.wpilibj.Joystick;
//import edu.wpi.first.wpilibj.Joystick.AxisType;


public class Robot_PositionClosedLoop extends IterativeRobot
{

//    CANTalon talon = new CANTalon(0);
//    Joystick joystick = new Joystick(0);
//    StringBuilder stringBuilder = new StringBuilder();
//    int _loops = 0;
//    boolean wasLastButton1 = false;
//    /** save the target position to servo to */
//    double targetPositionRotations;
//
//
//    public void robotInit()
//    {
//        /* lets grab the 360 degree position of the MagEncoder's absolute position */
//        int absolutePosition = talon.getPulseWidthPosition() & 0xFFF; /* mask out the bottom12 bits, we don't care about the wrap arounds */
//        /* use the low level API to set the quad encoder signal */
//        talon.setEncPosition(talon.getPulseWidthPosition() & 0xFFF);
//
//        /* choose the sensor and sensor direction */
//        talon.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);
//        talon.reverseSensor(false);
//        //talon.configEncoderCodesPerRev(XXX), // if using FeedbackDevice.QuadEncoder
//        //talon.configPotentiometerTurns(XXX), // if using FeedbackDevice.AnalogEncoder or AnalogPot
//
//        /* set the peak and nominal outputs, 12V means full */
//        talon.configNominalOutputVoltage(+0f, -0f);
//        talon.configPeakOutputVoltage(+12f, -12f);
//        /* set the allowable closed-loop error,
//         * Closed-Loop output will be neutral within this range.
//         * See Table in Section 17.2.1 for native units per rotation.
//         */
//        talon.setAllowableClosedLoopErr(0); /* always servo */
//        /* set closed loop gains in slot0 */
//        talon.setProfile(0);
//        talon.setF(0.0);
//        talon.setP(0.1);
//        talon.setI(0.0);
//        talon.setD(0.0);
//
//    }
//
//
//    /**
//     * This function is called periodically during operator control
//     */
//    public void teleopPeriodic()
//    {
//        /* get gamepad axis */
//        double leftYstick = joystick.getAxis(AxisType.kY);
//        double motorOutput = talon.getOutputVoltage() / talon.getBusVoltage();
//        boolean button1 = joystick.getRawButton(1);
//        boolean button2 = joystick.getRawButton(2);
//        /* prepare line to print */
//        stringBuilder.append("\tout:");
//        stringBuilder.append(motorOutput);
//        stringBuilder.append("\tpos:");
//        stringBuilder.append(talon.getPosition());
//        /* on button1 press enter closed-loop mode on target position */
//        if (!wasLastButton1 && button1)
//        {
//            /* Position mode - button just pressed */
//            targetPositionRotations = leftYstick * 50.0; /* 50 Rotations in either direction */
//            talon.changeControlMode(TalonControlMode.Position);
//            talon.set(targetPositionRotations); /* 50 rotations in either direction */
//
//        }
//        /* on button2 just straight drive */
//        if (button2)
//        {
//            /* Percent voltage mode */
//            talon.changeControlMode(TalonControlMode.PercentVbus);
//            talon.set(leftYstick);
//        }
//        /* if Talon is in position closed-loop, print some more info */
//        if (talon.getControlMode() == TalonControlMode.Position)
//        {
//            /* append more signals to print when in speed mode. */
//            stringBuilder.append("\terrNative:");
//            stringBuilder.append(talon.getClosedLoopError());
//            stringBuilder.append("\ttrg:");
//            stringBuilder.append(targetPositionRotations);
//        }
//        /* print every ten loops, printing too much too fast is generally bad for performance */
//        if (++_loops >= 10)
//        {
//            _loops = 0;
//            System.out.println(stringBuilder.toString());
//        }
//        stringBuilder.setLength(0);
//        /* save button state for on press detect */
//        wasLastButton1 = button1;
//    }
}
