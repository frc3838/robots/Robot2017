package frc.team3838.game.commands.drive;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.commands.drive.AbstractDriveForwardSetDistanceCommand;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;



public abstract class Abstract2017DriveForwardSetDistanceCommand extends AbstractDriveForwardSetDistanceCommand
{

    protected Abstract2017DriveForwardSetDistanceCommand(double targetDistanceInInches)
    {
        super(targetDistanceInInches);
    }

    protected Abstract2017DriveForwardSetDistanceCommand(double targetDistanceInInches, boolean enableTuning)
    {
        super(targetDistanceInInches, enableTuning);
    }


    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(Abstract2017DriveForwardSetDistanceCommand.class);


    @Override
    protected void initializeImpl() throws Exception
    {
        super.initializeImpl();
        logger.info("{} Encoder readings at Start: Left = {}; Right = {}",
                    getName(),
                    MathUtils.formatNumber(getDriveTrainSubsystem().getLeftDistanceInInches(), 3),
                    MathUtils.formatNumber(getDriveTrainSubsystem().getRightDistanceInInches(), 3));
    }


    @Override
    protected void endImpl() throws Exception
    {
        super.endImpl();
        logger.info("{} Encoder readings at End:   Left = {}; Right = {}",
                    getName(),
                    MathUtils.formatNumber(getDriveTrainSubsystem().getLeftDistanceInInches(), 3),
                    MathUtils.formatNumber(getDriveTrainSubsystem().getRightDistanceInInches(), 3));

    }


    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem() { return DriveTrainSubsystem.getInstance(); }


    @Nonnull
    @Override
    protected Abstract3838NavxSubsystem getNavxSubsystem() { return NavxSubsystem.getInstance(); }


    @Override
    protected double getMaxSpeed()
    {
        return 0.37;
    }


    @Override
    protected double getMinSpeed()
    {
        return 0.22;
    }

}
