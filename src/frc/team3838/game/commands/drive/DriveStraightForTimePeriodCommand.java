package frc.team3838.game.commands.drive;

import javax.annotation.Nonnull;

import frc.team3838.core.commands.drive.AbstractDriveStraightForTimePeriodCommand;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;



@API
public class DriveStraightForTimePeriodCommand extends AbstractDriveStraightForTimePeriodCommand
{
    @API
    public DriveStraightForTimePeriodCommand(double runTimeInMilliseconds)
    {
        super(runTimeInMilliseconds);
    }


    @API
    public DriveStraightForTimePeriodCommand(double runTimeInMilliseconds, boolean enableTuning)
    {
        super(runTimeInMilliseconds, enableTuning);
    }


    @API
    public DriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed)
    {
        super(runTimeInMilliseconds, speed);
    }


    @API
    public DriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed, boolean enableTuning)
    {
        super(runTimeInMilliseconds, speed, enableTuning);
    }


    @API
    public DriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection)
    {
        super(runTimeInMilliseconds, speed, robotDirection);
    }


    @API
    public DriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection, boolean enableTuning)
    {
        super(runTimeInMilliseconds, speed, robotDirection, enableTuning);
    }


    @API
    public DriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds)
    {
        super(name, runTimeInMilliseconds);
    }


    @API
    public DriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, boolean enableTuning)
    {
        super(name, runTimeInMilliseconds, enableTuning);
    }


    @API
    public DriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed)
    {
        super(name, runTimeInMilliseconds, speed);
    }


    @API
    public DriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed, boolean enableTuning)
    {
        super(name, runTimeInMilliseconds, speed, enableTuning);
    }


    @API
    public DriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection)
    {
        super(name, runTimeInMilliseconds, speed, robotDirection);
    }


    @API
    public DriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection, boolean enableTuning)
    {
        super(name, runTimeInMilliseconds, speed, robotDirection, enableTuning);
    }


    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem() { return DriveTrainSubsystem.getInstance(); }


    @Nonnull
    @Override
    protected Abstract3838NavxSubsystem getNavxSubsystem() { return NavxSubsystem.getInstance(); }
}
