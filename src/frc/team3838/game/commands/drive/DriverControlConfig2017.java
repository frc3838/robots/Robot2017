package frc.team3838.game.commands.drive;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.controls.AxisPair;
import frc.team3838.core.controls.NoOpAxisPair;
import frc.team3838.core.controls.NoOpThrottle;
import frc.team3838.core.controls.NoOpTrigger;
import frc.team3838.core.controls.Throttle;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.core.subsystems.drive.DriveTrainControlConfigImpl;



public class DriverControlConfig2017 extends DriveTrainControlConfigImpl
{
    @Nonnull
    private final Throttle climbMotorThrottle;
    @Nonnull
    private final Trigger allowReverseClimbTrigger;

    private static DriverControlConfig2017 noOpInstance;

    public DriverControlConfig2017(@Nonnull String configurationName,
                                   @Nonnull DriveControlMode driveControlMode,
                                   @Nonnull AxisPair axisPair,
                                   @Nullable Trigger reverseModeTrigger,
                                   @Nullable Trigger fineControlTrigger,
                                   @Nonnull Throttle climbMotorThrottle,
                                   @Nonnull Trigger allowReverseClimbTrigger)
    {
        super(configurationName, driveControlMode, axisPair, reverseModeTrigger, fineControlTrigger);
        this.climbMotorThrottle = climbMotorThrottle;
        this.allowReverseClimbTrigger = allowReverseClimbTrigger;
    }

    @API
    public static DriverControlConfig2017 getDriverControlConfig2017NoOpInstance()
    {
        if (noOpInstance == null)
        {
            noOpInstance = new DriverControlConfig2017("No Op DriverControlConfig2017",
                                                       DriveControlMode.Arcade,
                                                       new NoOpAxisPair(),
                                                       new NoOpTrigger(),
                                                       new NoOpTrigger(),
                                                       NoOpThrottle.getInstance(),
                                                       new NoOpTrigger());
        }
        return noOpInstance;
    }


    @Nonnull
    public Throttle getClimbMotorThrottle()
    {
        return climbMotorThrottle;
    }


    @Nonnull
    public Trigger getAllowReverseClimbTrigger()
    {
        return allowReverseClimbTrigger;
    }


    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("configurationName", getConfigurationName())
            .append("driveControlMode", getDriveControlMode())
            .append("axisPair", getAxisPair())
            .append("reverseModeTrigger", getReverseModeTrigger())
            .append("fineControlTrigger", getFineControlTrigger())
            .append("climbMotorThrottle", climbMotorThrottle)
            .append("allowReverseClimbTrigger", allowReverseClimbTrigger)
            .toString();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DriverControlConfig2017 that = (DriverControlConfig2017) o;

        return new EqualsBuilder()
            .appendSuper(super.equals(o))
            .append(climbMotorThrottle, that.climbMotorThrottle)
            .append(allowReverseClimbTrigger, that.allowReverseClimbTrigger)
            .isEquals();
    }


    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
            .appendSuper(super.hashCode())
            .append(climbMotorThrottle)
            .append(allowReverseClimbTrigger)
            .toHashCode();
    }
}
