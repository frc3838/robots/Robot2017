package frc.team3838.game.commands.drive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class DriveStraightSetDistanceCommand extends Abstract2017DriveForwardSetDistanceCommand
{
    private static final Logger logger = LoggerFactory.getLogger(DriveStraightSetDistanceCommand.class);


    public DriveStraightSetDistanceCommand(double targetDistanceInInches)
    {
        super(targetDistanceInInches);
    }

    public DriveStraightSetDistanceCommand(double targetDistanceInInches, boolean enableTuning)
    {
        super(targetDistanceInInches, enableTuning);
    }
}
