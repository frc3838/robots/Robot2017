package frc.team3838.game.commands.drive;

import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team3838.core.commands.common.SleepCommand;



public class DriveStraightSetDistanceWithDelayCommand extends CommandGroup
{
    public DriveStraightSetDistanceWithDelayCommand(double targetDistanceInInches, long delayInSeconds)
    {
        this (targetDistanceInInches, delayInSeconds, false);
    }


    public DriveStraightSetDistanceWithDelayCommand(double targetDistanceInInches, long delayInSeconds, boolean enableTuning)
    {
        addSequential(SleepCommand.createSeconds(delayInSeconds));
        addSequential(new DriveStraightSetDistanceCommand(targetDistanceInInches, enableTuning));
    }

    public DriveStraightSetDistanceWithDelayCommand(double targetDistanceInInches, @Nonnull String timeDelayDashboardKey, long defaultDelayValueInSeconds)
    {
        this(targetDistanceInInches, timeDelayDashboardKey, defaultDelayValueInSeconds, false);

    }

    public DriveStraightSetDistanceWithDelayCommand(double targetDistanceInInches, @Nonnull String timeDelayDashboardKey, long defaultDelayValueInSeconds, boolean enableTuning)
    {
        addSequential(SleepCommand.createReadFromDashboard(timeDelayDashboardKey, defaultDelayValueInSeconds));
        addSequential(new DriveStraightSetDistanceCommand(targetDistanceInInches, enableTuning));
    }
}
