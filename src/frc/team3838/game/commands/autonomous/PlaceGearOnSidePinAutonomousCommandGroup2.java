package frc.team3838.game.commands.autonomous;

import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team3838.core.commands.autonomous.AllianceAwareAutonomousCommandConfiguration;
import frc.team3838.core.commands.autonomous.AutonomousCommandConfiguration;
import frc.team3838.core.commands.common.SleepCommand;
import frc.team3838.core.commands.common.SleepCommand.PartialSeconds;
import frc.team3838.core.commands.log.LogCommandGroupFinishedCommand;
import frc.team3838.core.commands.log.LogCommandGroupStartCommand;
import frc.team3838.game.commands.drive.DrivePidRotateCommand;
import frc.team3838.game.commands.drive.DriveStraightSetDistanceCommand;



public class PlaceGearOnSidePinAutonomousCommandGroup2 extends CommandGroup
{
    private static final Command blueLeftNear =  new PlaceGearOnSidePinAutonomousCommandGroup2("Blue Left  [Near]", 90, 60, 32);
    private static final Command blueRightAway = new PlaceGearOnSidePinAutonomousCommandGroup2("Blue Right [Away]", 81, -60, 59);
    private static final Command redLeftAway =   new PlaceGearOnSidePinAutonomousCommandGroup2("Red  Left  [Away]", 81, 60, 59);
    private static final Command redRightNear =  new PlaceGearOnSidePinAutonomousCommandGroup2("Red  Right [Near]", 90, -60, 32);

    private static final AutonomousCommandConfiguration leftSideConfiguration = new AllianceAwareAutonomousCommandConfiguration("Gear on Left Pin", blueLeftNear, redLeftAway);
    private static final AutonomousCommandConfiguration rightSideConfiguration = new AllianceAwareAutonomousCommandConfiguration("Gear on Right Pin", blueRightAway, redRightNear);



    public static AutonomousCommandConfiguration getLeftSideConfiguration() { return leftSideConfiguration; }


    public static AutonomousCommandConfiguration getRightSideConfiguration() { return rightSideConfiguration; }


    private PlaceGearOnSidePinAutonomousCommandGroup2(@Nonnull String baseName, double preTurnDistance, double turnAngle, double postTurnDistance)
    {
        super(String.format("%s {%2.1f\", %2.1f degrees, %2.1f\"}", baseName, preTurnDistance, turnAngle, postTurnDistance));


        final LogCommandGroupStartCommand logCommandGroupStartCommand = new LogCommandGroupStartCommand(this);
        addSequential(logCommandGroupStartCommand);


        addSequential(new DriveStraightSetDistanceCommand(preTurnDistance));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.OneEighthSecond));

        addSequential(new DrivePidRotateCommand(turnAngle));
        addSequential(SleepCommand.createSeconds(0, PartialSeconds.OneQuarterSecond));

        addSequential(new DriveStraightSetDistanceCommand(postTurnDistance));


        addSequential(new LogCommandGroupFinishedCommand(this, logCommandGroupStartCommand));
    }

    /*

            Need 5 Pin options
                Center
                Blue Left  (Toward Steam)
                Blue Right (Away from Steam)
                Red  Left  (Away from Steam)
                Blue Right (Toward Steam)

            Blue - Steam on Left side of playing field
            Red -  Steam on Right side of playing field


            Condition 1: Red / Blue
            Condition 2: Toward or Away from Steam
                determines drive distances

            Center
                F 74"

     */
}
