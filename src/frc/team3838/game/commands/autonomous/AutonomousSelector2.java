package frc.team3838.game.commands.autonomous;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.autonomous.AutonomousCommandConfiguration;
import frc.team3838.core.commands.autonomous.AutonomousCommandConfigurationImpl;
import frc.team3838.core.commands.common.NoOpCommand;
import frc.team3838.core.smartdashboard.SortedSendableChooser;
import frc.team3838.core.smartdashboard.SortedSendableChooser.Order;
import frc.team3838.game.commands.drive.DrivePidRotateCommand;
import frc.team3838.game.commands.drive.DriveStraightSetDistanceCommand;
import frc.team3838.game.commands.drive.DriveStraightSetDistanceWithDelayCommand;



public class AutonomousSelector2
{
    private static final Logger logger = LoggerFactory.getLogger(AutonomousSelector2.class);

    public static final boolean INCLUDE_TEST_OPTIONS = false;

    public static final boolean ENABLE_DRIVE_STRAIGHT_TUNING = false;



    private static final String DELAY_DASHBOARD_KEY = "Autonomous Drive Straight Delay in Seconds";

    public static final SortedSendableChooser<AutonomousCommandConfiguration> chooser = new SortedSendableChooser<>(Order.Insertion);


    private static final int DRIVE_OVER_LINE_DISTANCE = 140;
    private static Command driveStraightOverLine = new DriveStraightSetDistanceCommand(DRIVE_OVER_LINE_DISTANCE, ENABLE_DRIVE_STRAIGHT_TUNING);
    private static Command driveStraightOverLineWithDelay = new DriveStraightSetDistanceWithDelayCommand(DRIVE_OVER_LINE_DISTANCE,
                                                                                                         DELAY_DASHBOARD_KEY,
                                                                                                         8,
                                                                                                         ENABLE_DRIVE_STRAIGHT_TUNING);

    private static final int CENTER_PIN_DISTANCE = 76;
    private static Command placeGearOnCenterPin = new DriveStraightSetDistanceCommand(CENTER_PIN_DISTANCE, ENABLE_DRIVE_STRAIGHT_TUNING);
    private static Command placeGearOnCenterPinWithDelay = new DriveStraightSetDistanceWithDelayCommand(CENTER_PIN_DISTANCE,
                                                                                                         "Autonomous Drive Straight Delay in Seconds",
                                                                                                         8,
                                                                                                         ENABLE_DRIVE_STRAIGHT_TUNING);



    // TEST COMMANDS
    private static AutonomousCommandConfiguration rotateNinetyCW =  new AutonomousCommandConfigurationImpl("TEST: Rotate 90 degrees CW", new DrivePidRotateCommand(90));
    private static AutonomousCommandConfiguration rotateNinetyCCW = new AutonomousCommandConfigurationImpl("TEST: Rotate -90 degrees CCW", new DrivePidRotateCommand(-90));
    private static AutonomousCommandConfiguration rotateFortyFive = new AutonomousCommandConfigurationImpl("TEST: Rotate 45 degrees", new DrivePidRotateCommand(45));
    private static AutonomousCommandConfiguration rotateSixtyCW =   new AutonomousCommandConfigurationImpl("TEST: Rotate  60 degrees CW", new DrivePidRotateCommand(60));
    private static AutonomousCommandConfiguration rotateSixtyCCW =  new AutonomousCommandConfigurationImpl("TEST: Rotate -60 degrees CCW", new DrivePidRotateCommand(-60));
//    private static AutonomousCommandConfiguration rotateVariable =  new AutonomousCommandConfigurationImpl("TEST: Rotate Variable as Set on Dashboard", new DrivePidRotateVariableSettingCommand());
    private static AutonomousCommandConfiguration driveStraight200 =  new AutonomousCommandConfigurationImpl("TEST: Drive Straight 200", new DriveStraightSetDistanceCommand(200, true));


    public static void initializeChooser()
    {
        try
        {
            logger.info("Setting up autonomous mode chooser...");

            // SIMPLE
            addChoice(new AutonomousCommandConfigurationImpl("Drive straight over line", driveStraightOverLine));
            addChoice(new AutonomousCommandConfigurationImpl("Drive straight over line with DELAY", driveStraightOverLineWithDelay));

            //PINS
            addChoice(PlaceGearOnSidePinAutonomousCommandGroup2.getLeftSideConfiguration());
            addChoice(new AutonomousCommandConfigurationImpl("Place gear on center pin", placeGearOnCenterPin));
            addChoice(new AutonomousCommandConfigurationImpl("Place gear on center pin with DELAY", placeGearOnCenterPinWithDelay));
            addChoice(PlaceGearOnSidePinAutonomousCommandGroup2.getRightSideConfiguration());


            // DEFAULT
            addDefault(new AutonomousCommandConfigurationImpl("DEFAULT: Do Nothing / No Autonomous Mode", NoOpCommand.getInstance()));

            if (INCLUDE_TEST_OPTIONS)
            {
                addChoice(rotateNinetyCW);
                addChoice(rotateNinetyCCW);
                addChoice(rotateFortyFive);
                addChoice(rotateSixtyCW);
                addChoice(rotateSixtyCCW);
                addChoice(driveStraight200);
            }


            SmartDashboard.putData("Autonomous Modes Selection...", chooser);
            logger.info("Autonomous mode chooser set up completed.");

        }
        catch (Throwable e)
        {
            final String msg = String.format("An error occurred when setting up Autonomous Chooser. Summary: %s", e.toString());
            DriverStation.reportError(msg, false);
            logger.error(msg, e);
        }
    }


    public static Command getSelectedCommand()
    {
        try
        {
            return chooser.getSelected().getCommand();
        }
        catch (Exception e)
        {
            final String msg = String.format("An error occurred selecting Autonomous Mode from chooser. Using NoOpCommand. Summary: %s", e.toString());
            DriverStation.reportError(msg, false);
            logger.error(msg, e);
            return NoOpCommand.getInstance();
        }
    }


    protected static void addChoice(AutonomousCommandConfiguration commandConfiguration)
    {
        chooser.addObject(commandConfiguration.getDescription(), commandConfiguration);
    }


    protected static void addDefault(AutonomousCommandConfiguration commandConfiguration)
    {
        chooser.addDefault(commandConfiguration.getDescription(), commandConfiguration);
    }
}
