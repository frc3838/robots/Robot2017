package frc.team3838.game;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.config.AbstractIOAssignments;
import frc.team3838.core.controls.BoostSpeedThrottle;
import frc.team3838.core.controls.ButtonAction;
import frc.team3838.core.controls.DualComboButton;
import frc.team3838.core.controls.GamepadTankAxisPair;
import frc.team3838.core.controls.HidBasedAxisPair;
import frc.team3838.core.controls.NoOpTrigger;
import frc.team3838.core.controls.Throttle;
import frc.team3838.core.controls.ThrottleBuilder;
import frc.team3838.core.controls.ThrottleBuilder.Directionality;
import frc.team3838.core.meta.API;
import frc.team3838.core.smartdashboard.SortedSendableChooser;
import frc.team3838.core.smartdashboard.SortedSendableChooser.Order;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.game.commands.camera.pi.SwitchToFrontPiCameraCommand;
import frc.team3838.game.commands.camera.pi.SwitchToRearPiCameraCommand;
import frc.team3838.game.commands.camera.pi.TogglePiCameraCommand;
import frc.team3838.game.commands.drive.DriverControlConfig2017;
import frc.team3838.game.subsystems.ClimberSubsystem;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.GearFeedSensorSubsystem;
import frc.team3838.game.subsystems.NavxSubsystem;
import frc.team3838.game.subsystems.piCamera.PiCameraSubsystem;



/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap
{
    /**
     * Factor added to AIO & DIO port and PWM Channel numbers on the MXP (myRIO Expansion Port) 'More Board'.
     * Thus DIO port 2 on the MXP More board is DIO 12 (2 + ADD_FACTOR) to the roboRIO.
     * And  AIO port 5 on the MXP More board is DIO 15 (5 + ADD_FACTOR) to the roboRIO.
     * <br/>
     * See <a href='www.revrobotics.com/product/more-board/'>www.revrobotics.com/product/more-board/</a> <br/>
     * See <a href='http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering'>http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering</a><br/>
     */
    @SuppressWarnings("unused")
    public static final int MXP_ADD_FACTOR = 10;

    // FLow sensor (mouse tracker) is on SPI


    @SuppressWarnings("WeakerAccess")
    @Nonnull
    public static final ImmutableMap<Class<? extends I3838Subsystem>, Boolean> enabledSubsystemsMap =
        ImmutableMap.of(
            GearFeedSensorSubsystem.class, true,
            ClimberSubsystem.class, true,
            DriveTrainSubsystem.class, true,
            NavxSubsystem.class, true,
            PiCameraSubsystem.class, true
        );


    public static class DIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please

        // Right Encoder DIOs - 0 & 1
        // Left  Encoder DIOs - 2 & 3
        public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_A = 0;
        public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_B = 1;

        public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_A = 2;
        public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_B = 3;

        // Used as a boolean indicator
        public static final int GEAR_SENSE = 4;

        // DIO 5 is used as an Input for its power, but we do not need to actual use/access it in code
        public static final int INPUT_USED_FOR_POWER = 5;

        public static final int CLIMB_REVERSE_PHYSICAL_SAFETY_SWITCH = 6;


        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_14 = 14;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_15 = 15;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_16 = 16;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_17 = 17;

        // Keep assignments in numerical order please
    }

    public static class AIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please

    }

    public static class PWMs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        public static final int CLIMBER_MOTOR = 0;

    }

    /**
     * Port assignments for the Pneumatic Control Module (PCM).
     * [Not to be confused with the PWMs (Pulse Width Modulation) channel assignment class.]
     */
    public static class PCM extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please

    }

    public static class Relays extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
    }

    public static class CANs extends AbstractIOAssignments
    {
        /*
        CAN IDs are assigned in the roboRIO UI -- http://172.22.11.2 -- **ALWAYS** assign as ID as they default to Zero
        So keep Zero "Free". See Section 2.2. "Common ID Talons" of the "Talon SRX Software Reference Manual.pdf"
        in C:\Users\Developer\Downloads\FRC\Hardware Components\Talon SRX or download from
        http://www.ctr-electronics.com/talon-srx.html#product_tabs_technical_resources
        From that Section:

            TIP: Since the default device ID of an “out of the box” Talon SRX is device ID ‘0’, when you setup your
                 robot for the first time, start assigning device IDs at ’1’. That way you can, at any time, add
                 another default Talon to your bus and easily identify it.
        */

        // Keep assignments in numerical order please
        @SuppressWarnings("unused")
        public static final int DEFAULT_AND_THEREFORE_RESERVED_ID = 0;

        public static final int DRIVE_LEFT_SIDE_REAR = 1;      // a.k.a. Driver's side
        public static final int DRIVE_LEFT_SIDE_FRONT = 2;
        public static final int DRIVE_RIGHT_SIDE_REAR = 3;     // a.k.a. Passenger's side
        public static final int DRIVE_RIGHT_SIDE_FRONT = 4;

        @SuppressWarnings("unused")
        public static final int PDP = 9;
    }

//    public static class UI_OLD
//    {
//        public static final Joystick gamepad = new Joystick(0);
//
//        public static final Trigger driveReversalTrigger = new JoystickButton(gamepad, 5);
//        public static final Trigger fineControlTrigger =
//            //new MultiPovButton(gamepad, PovDirection.N_UP, PovDirection.NW, PovDirection.NE);
//            new JoystickButton(gamepad, 6);
//
//        public static final Trigger allowReverseClimbTrigger = new JoystickButton(gamepad, 7);
//
//        // For the climber - a negative value is climb (i.e. forward)
//        public static final Throttle climberThrottle =
//            new ReversingThrottle(ThrottleBuilder.getBuilder()
//                                                 .using(gamepad)
//                                                 .usingRawAxis(2)
//                                                 .usingBiDirectionality()
//                                                 .usingNonInvertedOutput()
//                                                 .withoutDeadZone()
//                                                 .withoutScaling()
//                                                 .withoutActivationButton()
//                                                 .build(), allowReverseClimbTrigger);
//
//
//


    public static class UI
    {
        private static final Logger logger = LoggerFactory.getLogger(UI.class);

        public static final ImmutableList<DriverControlConfig2017> configs;
        private static final SortedSendableChooser<DriverControlConfig2017> configChooser;


        public static DriverControlConfig2017 getDriveTrainControlConfiguration()
        {
            return configChooser.getSelected();
        }


        static
        {

            final int INITIAL_THROTTLE_AXIS = 2;
            final int BOOST_THROTTLE_AXIS = 3;

            final Builder<DriverControlConfig2017> listBuilder = ImmutableList.builder();
            final Logger logger = LoggerFactory.getLogger(UI.class);

            final int gamepadToggleCameraButtonNumber = 1;
            final Command toggleCameraCommand = new TogglePiCameraCommand();
            final Command switchToRearCameraCommand = new SwitchToRearPiCameraCommand();
            final Command switchToFrontCameraCommand = new SwitchToFrontPiCameraCommand();

//            if (!HidUtils.isHidConnected(0) || !HidUtils.isGamepad(0))
//            {
//                logger.info("No gamepad detected on port 0. Will not create gamepad drive configuration options");
//            }
//            else
            {
                logger.info("Creating Gamepad DriveConfigurations");

                final int gamePadReverseModeButton = 5;
                final int gamePadFineControlButtonNum = 6;



                final XboxController gamepad = new XboxController(0);


                assignButton(gamepad, 2, ButtonAction.Button_WhenPressed, switchToFrontCameraCommand);
                assignButton(gamepad, 3, ButtonAction.Button_WhenPressed, switchToRearCameraCommand);
                assignButton(gamepad, gamepadToggleCameraButtonNumber, ButtonAction.Button_WhenPressed, toggleCameraCommand);


                final Button reverseModeTrigger = new JoystickButton(gamepad, gamePadReverseModeButton);
                reverseModeTrigger.whenPressed(switchToRearCameraCommand);
                reverseModeTrigger.whenReleased(switchToFrontCameraCommand);

                final Button fineControlTrigger = new JoystickButton(gamepad, gamePadFineControlButtonNum);
                //final Trigger fineControlTrigger =  new MultiPovButton(gamepad, PovDirection.N_UP, PovDirection.NW, PovDirection.NE);

                final Trigger allowReverseClimbTrigger = new NoOpTrigger();//new JoystickButton(gamepad, 7);


                // For the climber - a negative value is climb (i.e. forward)
                final Throttle initialThrottle = ThrottleBuilder.getBuilder()
                                                                .using(gamepad)
                                                                .usingRawAxis(INITIAL_THROTTLE_AXIS)
                                                                .usingPositiveDirection()
                                                                .outputPositiveValue()
                                                                .withoutDeadZone()
                                                                .withoutScaling()
                                                                .withoutActivationButton()
                                                                .build();

                final Throttle boostThrottle = ThrottleBuilder.getBuilder()
                                                              .using(gamepad)
                                                              .usingRawAxis(BOOST_THROTTLE_AXIS)
                                                              .usingPositiveDirection()
                                                              .outputPositiveValue()
                                                              .withoutDeadZone()
                                                              .withoutScaling()
                                                              .withoutActivationButton()
                                                              .build();

                // For the climber - a negative value is climb (i.e. forward)
                final Throttle climberThrottle = new BoostSpeedThrottle(initialThrottle, boostThrottle, ClimberSubsystem.MIN_CLIMB_ABS_SPEED, ClimberSubsystem.MAX_CLIMB_ABS_SPEED, Directionality.Negative);


//                // For the climber - a negative value is climb (i.e. forward)
//                final Throttle climberThrottle =
//                    new ReversingThrottle(ThrottleBuilder.getBuilder()
//                                                         .using(gamepad)
//                                                         .usingRawAxis(2)
//                                                         .usingBiDirectionality()
//                                                         .usingNonInvertedOutput()
//                                                         .withoutDeadZone()
//                                                         .withoutScaling()
//                                                         .withoutActivationButton()
//                                                         .build(), allowReverseClimbTrigger);


                final DriverControlConfig2017 gamepadArcade = new DriverControlConfig2017("Gamepad Arcade Control",
                                                                                          DriveControlMode.Arcade,
                                                                                          new HidBasedAxisPair(gamepad, 4, 5),
                                                                                          reverseModeTrigger,
                                                                                          fineControlTrigger,
                                                                                          climberThrottle,
                                                                                          allowReverseClimbTrigger);

                listBuilder.add(gamepadArcade);


                final DriverControlConfig2017 gamepadTank = new DriverControlConfig2017("Gamepad Tank Control",
                                                                                        DriveControlMode.Tank,
                                                                                        new GamepadTankAxisPair(gamepad),
                                                                                        reverseModeTrigger,
                                                                                        fineControlTrigger,
                                                                                        climberThrottle,
                                                                                        allowReverseClimbTrigger);
                listBuilder.add(gamepadTank);

            }


            final int joystickReverseModeButtonNumber = 3;
            final int joystickFineControlButtonNumber = 1;
            final int joystickToggleCameraButtonNumber = 7;

            final int joystickAllowReverseClimbNumberA = 8;
            final int joystickAllowReverseClimbNumberB = 9;

//            if (!HidUtils.isHidConnected(1))
//            {
//                logger.info("No joystick detected on port 1. Will not create joystick drive configuration options");
//            }
//            else
            {

                logger.info("Creating Joystick DriveConfigurations");

                final Joystick primaryStick = new Joystick(1);
                final Joystick altStick = new Joystick(2);

                assignButton(primaryStick, joystickToggleCameraButtonNumber, ButtonAction.Button_WhenPressed, toggleCameraCommand);
                final Button arcadeReverseModeTrigger = new JoystickButton(primaryStick, joystickReverseModeButtonNumber);
                arcadeReverseModeTrigger.whenPressed(switchToRearCameraCommand);
                arcadeReverseModeTrigger.whenReleased(switchToFrontCameraCommand);
                final Trigger arcadeFineControlTrigger = new JoystickButton(primaryStick, joystickFineControlButtonNumber);

                final Trigger arcadeAllowReverseClimbTrigger = new DualComboButton(new JoystickButton(primaryStick, joystickAllowReverseClimbNumberA),
                                                                                   new JoystickButton(primaryStick, joystickAllowReverseClimbNumberB));
                // For the climber - a negative value is climb (i.e. forward)
                final Throttle arcadeClimberThrottle = ThrottleBuilder.getBuilder()
                                                                      .using(altStick)
                                                                      .usingYAxis()
                                                                        .usingUpPushNegativeDirection()
                                                                        .outputNegativeValue()
                                                                        .withDeadZoneThreshold(0.19)
                                                                        .withAbsoluteScaling(ClimberSubsystem.MIN_CLIMB_ABS_SPEED, ClimberSubsystem.MAX_CLIMB_ABS_SPEED, 0, 1)
                                                                        .withoutActivationButton()
                                                                        .build();


                final DriverControlConfig2017 joystickArcade =
                    new DriverControlConfig2017("Joystick Arcade Control",
                                                DriveControlMode.Arcade,
                                                new HidBasedAxisPair(primaryStick),
                                                arcadeReverseModeTrigger,
                                                arcadeFineControlTrigger,
                                                arcadeClimberThrottle,
                                                arcadeAllowReverseClimbTrigger
                    );
                listBuilder.add(joystickArcade);
//
//                if (!HidUtils.isHidConnected(2))
//                {
//                    logger.info("No joystick detected on port 2. Will not create joystick tank drive configuration option");
//                }
//                else
                {
//
//                    assignButton(rightStick, joystickToggleCameraButtonNumber, ButtonAction.Button_WhenPressed, toggleCameraCommand);
//
//                    final Trigger tankReverseModeTrigger = new JoystickButton(rightStick, joystickReverseModeButtonNumber);
//                    final Trigger tankFineControlTrigger = new JoystickButton(rightStick, joystickFineControlButtonNumber);
//
//                    final Trigger tankAllowReverseClimbTrigger = new DualComboButton(new JoystickButton(rightStick, joystickAllowReverseClimbNumberA),
//                                                                                     new JoystickButton(rightStick, joystickAllowReverseClimbNumberB));
//                    // For the climber - a negative value is climb (i.e. forward)
//                    final Throttle tankClimberThrottle = ThrottleBuilder.getBuilder()
//                                                                        .using(rightStick)
//                                                                        .usingYAxis()
//                                                                        .usingUpPushNegativeDirection()
//                                                                        .outputNegativeValue()
//                                                                        .withoutDeadZone()
//                                                                        .withAbsoluteScaling(MIN_CLIMB_ABS_SPEED, MAX_CLIMB_ABS_SPEED, 0, 1)
//                                                                        .withoutActivationButton()
//                                                                        .build();
//
//
//                    final DriverControlConfig2017 joystickTank =
//                        new DriverControlConfig2017("Joystick Tank Control",
//                                                    DriveControlMode.Tank,
//                                                    new HidBasedAxisPair(rightStick),
//                                                    tankReverseModeTrigger,
//                                                    tankFineControlTrigger,
//                                                    tankClimberThrottle,
//                                                    tankAllowReverseClimbTrigger
//                        );
//
//
//                    listBuilder.add(joystickTank);
                }

            }

            configs = listBuilder.build();


            configChooser = new SortedSendableChooser<>(Order.Insertion);


            if (configs.isEmpty())
            {
                logger.error("No joystick or gamepad is connected. Cannot create a UI configuration.");
                final DriverControlConfig2017 noOpConfig = DriverControlConfig2017.getDriverControlConfig2017NoOpInstance();
                configChooser.addDefault("No operation - no joystick/gamepad is connected.", noOpConfig);
            }
            else
            {
                for (int i = 0; i < configs.size(); i++)
                {
                    final DriverControlConfig2017 config = configs.get(i);
                    final String prefix = "# " + (i + 1) + ") ";
                    if (i == 0)
                    {
                        configChooser.addDefault(prefix + config.getConfigurationName(), config);
                    }
                    else
                    {
                        configChooser.addObject(prefix + config.getConfigurationName(), config);
                    }
                }
            }

            SmartDashboard.putData("==UI Configuration Chooser==", configChooser);

        } // end static initializer


        @API
        public static void assignButton(final GenericHID hid,
                                        final int buttonNumber,
                                        final ButtonAction action,
                                        final Command command) throws IllegalArgumentException
        {
            final JoystickButton button = new JoystickButton(hid, buttonNumber);
            action.assign(button, command);
        }
    }
}
