package frc.team3838.game.subsystems;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.hardware.CANTalonPair;
import frc.team3838.core.hardware.EncoderConfig;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.game.Robot3838;
import frc.team3838.game.RobotMap.CANs;
import frc.team3838.game.RobotMap.DIOs;
import frc.team3838.game.RobotMap.UI;
import frc.team3838.game.commands.drive.DriverControlConfig2017;



public class DriveTrainSubsystem extends Abstract3838DriveTrainSubsystem
{
    private static final Logger logger = LoggerFactory.getLogger(DriveTrainSubsystem.class);

    private final double encoderDistancePerPulse = Robot3838.getEncoderDistancePerPulse();


    // TODO - need to configure
    // STALL SPEED is 0.17 in both directions


//    private MotorOps leftMotorOps;
//    private MotorOps rightMotorOps;


    private final ScheduledExecutorService scheduledExecutorService;



    protected DriveTrainSubsystem()
    {
        super(new CANTalonPair(CANs.DRIVE_LEFT_SIDE_REAR, CANs.DRIVE_LEFT_SIDE_FRONT),
              new CANTalonPair(CANs.DRIVE_RIGHT_SIDE_REAR, CANs.DRIVE_RIGHT_SIDE_FRONT),

              UI.getDriveTrainControlConfiguration()
        );
//        try
//        {
//            this.leftMotorOps = new MotorOps(getLeftSpeedController(), false);
//            this.rightMotorOps = new MotorOps(getRightSpeedController(), false);
//        }
//        catch (Exception e)
//        {
//            logger.error("An exception occurred when initialing MotorOps fields in DriveTrainSubsystem constructor. "
//                         + "DriveTrainSubsystem will be disabled. Cause Summary: {}", e.toString(), e);
//            disableSubsystem();
//        }
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            final DriverControlConfig2017 latestConfig = UI.getDriveTrainControlConfiguration();
            if (!getDriveTrainControlConfig().equals(latestConfig))
            {
                setDriveTrainControlConfig(latestConfig);
                logger.info("Switching Control Mode to {}", latestConfig);
            }

        }, 0, 20, TimeUnit.MILLISECONDS);

    }



    @Override
    protected void initSubsystem() throws Exception
    {
        logger.debug("DriveTrainSubsystem.initSubsystem() running");
        setCurveFactor(2);
        setShouldCurveInput(true);
        initEncoders();
        logger.debug("DriveTrainSubsystem.initSubsystem() completed");
    }


    @Nullable
    @Override
    protected EncoderConfig getLeftEncoderConfig()
    {
        return new EncoderConfig(DIOs.DRIVE_TRAIN_LEFT_ENCODER_CH_A, DIOs.DRIVE_TRAIN_LEFT_ENCODER_CH_B, encoderDistancePerPulse, true);
    }


    @Nullable
    @Override
    protected EncoderConfig getRightEncoderConfig()
    {
        return new EncoderConfig(DIOs.DRIVE_TRAIN_RIGHT_ENCODER_CH_A, DIOs.DRIVE_TRAIN_RIGHT_ENCODER_CH_B, encoderDistancePerPulse, false);
    }



    @SuppressWarnings({"MethodDoesntCallSuperMethod", "RedundantMethodOverride"})
    protected double getFineControlMaxAbsoluteSpeed()
    {
        return getDriveTrainControlConfig().getDriveControlMode() == DriveControlMode.Arcade ? 0.5 : 0.57;
    }



    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final DriveTrainSubsystem singleton = new DriveTrainSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static DriveTrainSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}
