package frc.team3838.game.subsystems;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.annotation.Nonnull;

import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838UsbCameraSubsystem;



public class UsbCameraSubsystem extends Abstract3838UsbCameraSubsystem
{
    protected UsbCameraSubsystem()
    {
        super();
    }


    private final static int FRONT_CAMERA_DEVICE_NUMBER = 0;
    private final static int REAR_CAMERA_DEVICE_NUMBER = 1;
    
    private static int activeCamera = FRONT_CAMERA_DEVICE_NUMBER;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    
    

    @Override
    protected int getDefaultCameraDeviceNumber() { return FRONT_CAMERA_DEVICE_NUMBER; }


    @Override
    protected CameraOperationMode getCameraImplMode() { return CameraOperationMode.Simple; }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected int getResolutionWidthSetting() { return 320; }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected int getResolutionHeightSetting() { return 240; }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected int getFpsSetting() { return 30; }
    
    

    @API
    public void switchToFrontCamera()
    {
        switchToCamera(FRONT_CAMERA_DEVICE_NUMBER);
    }
    
    @API
    public void switchToRearCamera()
    {
        switchToCamera(REAR_CAMERA_DEVICE_NUMBER);
    }
    
    private void switchToCamera(int newCameraDeviceNumber)
    {
        lock.writeLock().lock();
        try
        {
            if (!(activeCamera == newCameraDeviceNumber))
            {
                switchCameras(activeCamera, newCameraDeviceNumber);
                activeCamera = newCameraDeviceNumber;
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }


    @API
    public void toggleCamera()
    {
        lock.writeLock().lock();
        try
        {
            if ((activeCamera == FRONT_CAMERA_DEVICE_NUMBER))
            {
                switchCameras(activeCamera, REAR_CAMERA_DEVICE_NUMBER);
                activeCamera = REAR_CAMERA_DEVICE_NUMBER;
            }
            else
            {
                switchCameras(activeCamera, FRONT_CAMERA_DEVICE_NUMBER);
                activeCamera = FRONT_CAMERA_DEVICE_NUMBER;
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }









    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final UsbCameraSubsystem singleton = new UsbCameraSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static UsbCameraSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */

}
