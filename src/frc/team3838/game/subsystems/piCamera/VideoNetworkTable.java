// Written bu Kurt M. Sanger for Team 3838
// Created 2/15/2017 for FRC 2017 SteamWorks Game
//
// Network Table for communicating to vision subsystem on Raspberry Pi
//
//  Camera0 is front facing usb camera used for target detection and driving.
//  Camera1 is rear facing usb camera for aligning to the rope for climbing.
//
// Video Mode camera0 is default mode for driving forward.
// Camera is set to deliver a good visible image.
// Video Mode camera1 is mode for driving in reverse with second camera.
// Video Mode acquireTarget changes camera settings to better id target.
// assuming camera settings are adjustable.

package frc.team3838.game.subsystems.piCamera;

import edu.wpi.first.wpilibj.networktables.NetworkTable;

public class VideoNetworkTable {
	NetworkTable videoNT;

	private final static double kvm0 = 0.0;
	private final static double kvm1 = 1.0;
	private final static double kvm2 = 2.0;

	public enum videoMode {
		camera0("camera0", kvm0),  // Forward Camera
		camera1("camera1", kvm1),  // Backward Camera
		//camera2("camera2", kvm2);  // Forward Targeting Camera Not Available
		;

		private final String strVideoMode;
		private final double value;
		videoMode(String strVideoMode, double value) {
			this.strVideoMode = strVideoMode;
			this.value = value;
        }
		public String getString() {return strVideoMode; }
		public double getValue() { return value; }
	}

	// Video Mode camera0 is default mode for driving forward.
	// Camera is set to deliver a good visible image.
	// Video Mode camera1 is mode for driving in reverse with second camera.
	// Video Mode camera2 is the targeting camera image.

	public VideoNetworkTable() {

		String userName = System.getProperty("user.name","user.name UNDEFINED");

		if (userName.equals("lvuser")) {
			// We are running on the roboRio.  Init Table.
			initVideoNT();
		} else {
			// We are running somewhere else.  Init Client mode.
			initVideoNTClient();
		}
	}

	private void initVideoNTClient() {
		// Used on the pi.
	    // Connect NetworkTables, and get access to the publishing table
		System.out.println("Setting Up Video Network Table Client");
	    NetworkTable.setClientMode();
	    // Set your team number here
	    NetworkTable.setTeam(3838);

	    NetworkTable.initialize();

	    try {
			Thread.sleep(2000);  // give NetworkTable a chance to init.
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    videoNT = NetworkTable.getTable("videotable");
	    //if (videoNT.getKeys().size() > 0) {
	    	// We connected to network tables.
	    	//setPiReady();
	    //}
	}




	private void initVideoNT()  {
		// Create Video Network Table
		System.out.println("Setting Up Video Network Table");
		videoNT = NetworkTable.getTable("videotable");

		// Prepopulate table with defaults.
		videoNT.putBoolean("pi ready", false);      // pi sets true when running and Network Table Found.
		videoNT.putNumber("videoMode", videoMode.camera0.getValue()); // sets pi video stream source.
		videoNT.putBoolean("camera0 found", false);  // set by pi if camera0 is found.
		videoNT.putBoolean("camera1 found", false);  // set by pi if camera1 is found.
		videoNT.putBoolean("camera2 found", false);  // set by pi if camera1 is found.
		// Note below values are overwritten by pi during image processing.
		videoNT.putNumber("TargetAngle", -180);		 // Target Angle in Degrees.
		videoNT.putNumber("TargetDistance",  -1);	 // Distance to Target in inches.
		videoNT.putNumber("TargetCentroid", -1);	 // Target Center in Pixels.
		videoNT.putNumber("TargetWidth",  -1);       // Target Width in Pixels.
		videoNT.putNumber("TargetHeight", -1);       // Target Height in Pixels.
		videoNT.putNumber("LeftTargetCentroid", -1); // Left Target Center in Pixels.
		videoNT.putNumber("LeftTargetWidth",  -1);   // Left Target Width in Pixels.
		videoNT.putNumber("LeftTargetHeight", -1);   // Left Target Height in Pixels.
		videoNT.putNumber("RightTargetCentroid", -1); // Right Target Center in Pixels.
		videoNT.putNumber("RightTargetWidth",  -1);   // Right Target Width in Pixels.
		videoNT.putNumber("RightTargetHeight", -1);   // Right Target Height in Pixels.
		videoNT.putBoolean("SnapTargetImage", false); // Take a picture of the target image.
	}

	public void setPiReady(){
		videoNT.putBoolean("pi ready", true);
	}

	public void resetPiReady(){
		videoNT.putBoolean("pi ready", false);
	}

	public boolean getPiReady(){
		return videoNT.getBoolean("pi ready", false);
	}

	public void setVideoMode(videoMode vm){
		videoNT.putNumber("videoMode", vm.getValue());
	}

	public double getVideoMode(){
		return videoNT.getNumber("videoMode",-1.0);
	}

	public String getVideoModeStr(){
		int testMode;
		testMode = (int) videoNT.getNumber("videoMode",-1.0);

		switch (testMode) {
		case ((int) kvm0) :
			return videoMode.camera0.getString();
		case ((int) kvm1) :
			return videoMode.camera1.getString();
		//case ((int) kvm2) :
			//return videoMode.camera2.getString();
		default :
			return "UnknownVideoMode";
		}
	}

	public void setCamera0Found(){
		videoNT.putBoolean("camera0 found",true);
	}

	public void setCamera0NotFound(){
		videoNT.putBoolean("camera0 found", false);
	}

	public void setCamera1Found(){
		videoNT.putBoolean("camera1 found",true);
	}

	public void setCamera1NotFound(){
		videoNT.putBoolean("camera1 found", false);
	}

	public void setCamera2Found(){
		videoNT.putBoolean("camera2 found",true);
	}

	public void setCamera2NotFound(){
		videoNT.putBoolean("camera2 found", false);
	}

	public boolean getCamera0Found(){
		return videoNT.getBoolean("camera0 found",false);
	}

	public boolean getCamera1Found(){
		return videoNT.getBoolean("camera1 found",false);
	}

	public boolean getCamera2Found(){
		return videoNT.getBoolean("camera2 found",false);
	}

	public void setTargetAngle(double ta){
		videoNT.putNumber("TargetAngle", ta);
	}

	public void setTargetCentroid(double tc){
		videoNT.putNumber("TargetCentroid", tc);
	}

	public double getTargetAngle(){
		return videoNT.getNumber("TargetAngle",-360);  // -360 if key does not exist.
	}

	public void setTargetDistance(double td){
		videoNT.putNumber("TargetDistance", td);
	}

	public double getTargetDistance(){
		return videoNT.getNumber("TargetDistance",-2);  // -2 if key does not exist.
	}


	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getTargetCentroid(){
		return videoNT.getNumber("TargetCentroid",-2.0);
	}

	public void setTargetWidth(double tc){
		videoNT.putNumber("TargetWidth", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getTargetWidth(){
		return videoNT.getNumber("TargetWidth",-2.0);
	}

	public void setTargetHeight(double tc){
		videoNT.putNumber("TargetHeight", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getTargetHeight(){
		return videoNT.getNumber("TargetHeight",-2.0);
	}

	public void setLeftTargetCentroid(double tc){
		videoNT.putNumber("LeftTargetCentroid", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getLeftTargetCentroid(){
		return videoNT.getNumber("LeftTargetCentroid",-2.0);
	}

	public void setLeftTargetWidth(double tc){
		videoNT.putNumber("LeftTargetWidth", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getLeftTargetWidth(){
		return videoNT.getNumber("LeftTargetWidth",-2.0);
	}

	public void setLeftTargetHeight(double tc){
		videoNT.putNumber("LeftTargetHeight", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getLeftTargetHeight(){
		return videoNT.getNumber("LeftTargetHeight",-2.0);
	}

	public void setRightTargetCentroid(double tc){
		videoNT.putNumber("RightTargetCentroid", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getRightTargetCentroid(){
		return videoNT.getNumber("RightTargetCentroid",-2.0);
	}

	public void setRightTargetWidth(double tc){
		videoNT.putNumber("RightTargetWidth", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getRightTargetWidth(){
		return videoNT.getNumber("RightTargetWidth",-2.0);
	}

	public void setRightTargetHeight(double tc){
		videoNT.putNumber("RightTargetHeight", tc);
	}

	// Return -2.0 if table identifier string is not found.
	// verses returning -1.0 if no target is identified.
	public double getRightTargetHeight(){
		return videoNT.getNumber("RightTargetHeight",-2.0);
	}

	public void resetTargetData(){
		// Sets all targeting data to -1.0 to indicate no target identified.
		this.setTargetAngle(-180);
		this.setTargetDistance(-1);
		this.setTargetCentroid(-1.0);
		this.setTargetHeight(-1.0);
		this.setTargetWidth(-1);
		this.setLeftTargetCentroid(-1.0);
		this.setLeftTargetHeight(-1.0);
		this.setLeftTargetWidth(-1.0);
		this.setRightTargetCentroid(-1.0);
		this.setRightTargetHeight(-1.0);
		this.setRightTargetWidth(-1.0);
	}

	public boolean videoNetworkTablesIsConnected() {
		return (videoNT.getKeys().size()>0);
	}

	public boolean getSnapTargetImage(){
		return videoNT.getBoolean("SnapTargetImage",false);
	}

	public void setSnapTargetImage(){
		// take a picture
		videoNT.putBoolean("SnapTargetImage", true);
	}

	public void resetSnapTargetImage(){
		// take a picture
		videoNT.putBoolean("SnapTargetImage", false);
	}

}
