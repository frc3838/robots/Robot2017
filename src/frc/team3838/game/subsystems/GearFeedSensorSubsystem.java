package frc.team3838.game.subsystems;

import java.util.Set;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.game.RobotMap.DIOs;



public class GearFeedSensorSubsystem extends Abstract3838Subsystem
{
    // We do not need to actually access or use this in code. Hist create it so it gets power
    private DigitalInput powerDI_noAccessNeeded;
    private DigitalInput gearSensor;

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private GearFeedSensorSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem
        powerDI_noAccessNeeded = new DigitalInput(DIOs.INPUT_USED_FOR_POWER);
        gearSensor = new DigitalInput(DIOs.GEAR_SENSE);

        LiveWindow.addSensor(getName(), "gearSensor DigitalInput  [" + DIOs.GEAR_SENSE + ']', gearSensor);
        LiveWindow.addSensor(getName(), "digitalInput (for Power) [" + DIOs.INPUT_USED_FOR_POWER + ']', powerDI_noAccessNeeded);
    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here.
        // A default command runs without any driver action
        // Default Commands are not common for most subsystems
        setDefaultCommand(new Abstract3838Command() {

            private void updateSmartdashboard()
            {
                SmartDashboard.putBoolean(" Have Gear ", gearSensor.get());
                //SmartDashboard.putBoolean("DI: ", powerDI_noAccessNeeded.get());
            }

            @Nonnull
            @Override
            protected Set<I3838Subsystem> getRequiredSubsystems() { return ImmutableSet.of(getInstance()); }


            @Override
            protected void initializeImpl() throws Exception
            {
                updateSmartdashboard();
            }





            @Override
            protected void executeImpl() throws Exception
            {
                updateSmartdashboard();
            }


            @Override
            protected boolean isFinishedImpl() throws Exception { return false; }


            @Override
            protected void endImpl() throws Exception { }
        });
    }
















    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final GearFeedSensorSubsystem singleton = new GearFeedSensorSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static GearFeedSensorSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

