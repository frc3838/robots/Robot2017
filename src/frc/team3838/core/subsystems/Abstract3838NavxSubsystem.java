package frc.team3838.core.subsystems;

import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.SPI;
import frc.team3838.core.meta.API;
import frc.team3838.core.utils.MathUtils;



public abstract class Abstract3838NavxSubsystem extends Abstract3838Subsystem
{

    /*
        ***IMPORTANT NOTE***
            *** Due to a board layout issue on the navX-MXP, there is noticeable ***
            *** crosstalk between ANALOG IN pins 3, 2 and 1. For that reason,    ***
            *** use of pin 3 and pin 2 is NOT RECOMMENDED.                       ***
     */


    protected final Logger logger = LoggerFactory.getLogger(getClass());


    @Nullable
    private AHRS navx;

    private boolean initHasRun = false;

    protected Abstract3838NavxSubsystem()
    {
        this(0);
    }

    /**
     * This is a protected constructor that should be called from a private constructor in the implmenting
     * class to ensure a single instance (i.e. singleton pattern).
     * Use the static {@code #getInstance()} method from the implementing class to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     NavxSubsystem subsystem = new NavxSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     NavxSubsystem subsystem = NavxSubsystem.getInstance();
     * </pre>
     *
     * @param angleAdjustment Sets an amount of angle to be automatically added before returning a angle from the getAngle() method.
     *                        This allows users of the getAngle() method to logically rotate the sensor by a given amount of degrees.
     *                        <strong>NOTE 1:</strong> The adjustment angle is only applied to the value returned from getAngle() -
     *                        it does not adjust the value returned from getYaw(), nor any of the quaternion values.
     *                        <strong>NOTE 2:</strong> The adjustment angle is not automatically cleared whenever the sensor yaw angle
     *                        is reset. If not set, the default adjustment angle is 0 degrees (no adjustment).
     */
    protected Abstract3838NavxSubsystem(double angleAdjustment)
    {
        super();
        if (isEnabled())
        {
            try
            {
                logger.info("Initializing {} with adjustment angle of {}", getClass().getSimpleName(), angleAdjustment);
                final long startTime = System.currentTimeMillis();
                final byte UPDATE_RATE_HZ = (byte) 200;
                navx = new AHRS(SPI.Port.kMXP, UPDATE_RATE_HZ);
                navx.setAngleAdjustment(angleAdjustment);


                // Disable the *highly* verbose tract
                try
                {
                    final Field ioField = navx.getClass().getDeclaredField("io");
                    ioField.setAccessible(true);
                    final Object IIOProviderInstance = ioField.get(navx);
                    final Field ioProviderField = IIOProviderInstance.getClass().getDeclaredField("io_provider");
                    ioProviderField.setAccessible(true);
                    final Object ioProviderInstance = ioProviderField.get(IIOProviderInstance);
                    final Field traceField = ioProviderInstance.getClass().getDeclaredField("trace");
                    traceField.setAccessible(true);
                    traceField.set(ioProviderInstance, false);
                    logger.info("Navx 'trace' reflectively set to false");
                }
                catch (Exception e)
                {
                    logger.info("Could not disable 'trace' in the NavX AHRS. Cause summary: {}", e.toString());
                }


                logger.info("Waiting for navX to calibrate.");
                while (navx.isCalibrating())
                {
                    final int timeOutInMs = 3_000;
                    if ((System.currentTimeMillis() - startTime) > timeOutInMs)
                    {
                        logger.warn("navX calibration did not complete within {}ms. "
                                    + "This likely means the navX board is not connected. "
                                    + "The {} will be DISABLED.", timeOutInMs, getClass().getSimpleName());
                        disableSubsystem();
                        break;
                    }
                    logger.info("navX is calibrating...");
                    try {TimeUnit.MILLISECONDS.sleep(500);} catch (InterruptedException ignore) {}
                }


                if (isEnabled() && navx != null)
                {
                    navx.reset();
                    logger.info("navX calibration completed. Initialization took {}ms", System.currentTimeMillis() - startTime);
                    logger.info("navX Firmware Version: {}", navx.getFirmwareVersion());

                    final String headerMsg = "navX Initialization Values";
                    logNavxValues(headerMsg);

                    logger.debug("{} initialization completed successfully", getClass().getSimpleName());
                }
                else
                {
                    if (isEnabled()) {disableSubsystem();}
                }
            }
            catch (Throwable e)
            {
                disableSubsystem();
                logger.error("An exception occurred in {} constructor. The Subsystem will be DISABLED.", getClass().getSimpleName(), e);
            }
            finally
            {
                initHasRun = true;
            }
        }
    }


    @Nullable
    public AHRS getNavx() { return navx; }


    public void logNavxValues(String headerMsg)
    {
        if (navx != null)
        {
            logger.info(headerMsg);
            logger.info("\tnavX altitude: {}", MathUtils.formatNumber(navx.getAltitude(), 4));
            logger.info("\tnavX angle: {}", MathUtils.formatNumber(navx.getAngle(), 4));
            logger.info("\tnavX CompassHeading: {}", MathUtils.formatNumber(navx.getCompassHeading(), 4));
            logger.info("\tnavX Fused Heading: {}", MathUtils.formatNumber(navx.getFusedHeading(), 4));
            logger.info("\tnavX Rate: {}", MathUtils.formatNumber(navx.getRate(), 4));
            logger.info("\tnavX Pitch: {}", MathUtils.formatNumber(navx.getPitch(), 4));
            logger.info("\tnavX Roll: {}", MathUtils.formatNumber(navx.getRoll(), 4));
            logger.info("\tnavX Yaw: {}", MathUtils.formatNumber(navx.getYaw(), 4));
            logger.info("\tnavX DisplacementX: {}", MathUtils.formatNumber(navx.getDisplacementX(), 4));
            logger.info("\tnavX DisplacementY: {}", MathUtils.formatNumber(navx.getDisplacementY(), 4));
            logger.info("\tnavX DisplacementZ: {}", MathUtils.formatNumber(navx.getDisplacementZ(), 4));
        }
    }

    @API
    public void logDisplacement(String statusMsg)
    {
        if (navx != null)
        {
            logger.info("Displacement (in meters) @ {}: x:{}; y={}; z={}",
                        statusMsg,
                        navx.getDisplacementX(),
                        navx.getDisplacementY(),
                        navx.getDisplacementZ());
        }
    }


    /**
     * For the NavxSubsystem, this will return true only if the subsystem is enabled, AND the navx is NOT null.
     *
     *  {@inheritDoc}
     *
     * @return true id the subsystem is enabled, and the navx is not null.
     */
    @Override
    public boolean isEnabled()
    {
        if (initHasRun)
        {
            return super.isEnabled() && navx != null  && navx.isConnected();
        }
        else
        {
            return super.isEnabled();
        }
    }


    @Override
    public void disableSubsystem()
    {
        if (navx != null)
        {
            navx.free();
            navx = null;
        }
        super.disableSubsystem();
    }
}
