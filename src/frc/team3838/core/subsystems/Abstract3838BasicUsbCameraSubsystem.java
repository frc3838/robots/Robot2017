package frc.team3838.core.subsystems;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;



public abstract class Abstract3838BasicUsbCameraSubsystem extends Abstract3838Subsystem
{

    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Abstract3838BasicUsbCameraSubsystem.class);


    protected Abstract3838BasicUsbCameraSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
    }

    protected String getCameraName()
    {
        return "cam" + getCameraPort();
    }
    protected abstract int getCameraPort();


    @Override
    protected void initSubsystem() throws Exception
    {
        initImpl();
    }


    protected void initImpl() throws Exception
    {

        // *********************************************************************
        //   On Smartdashboard, use "CameraServer Stream Viewer"

        // http://wpilib.screenstepslive.com/s/4485/m/24194/l/669166-using-the-camera-server-on-the-roborio-2017
        new Thread(() ->
                   {
                       final UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
                       final int width = getResolutionWidthSetting();
                       final int height = getResolutionHeightSetting();

                       camera.setResolution(width, height);
                       camera.setFPS(getFpsSetting());

                       CvSink cvSink = CameraServer.getInstance().getVideo();
                       CvSource outputStream = CameraServer.getInstance().putVideo("Blur", width, height);

                       Mat source = new Mat();
                       Mat output = new Mat();

                       while (!Thread.interrupted())
                       {
                           cvSink.grabFrame(source);
                           Imgproc.cvtColor(source, output, Imgproc.COLOR_BGR2GRAY);
                           outputStream.putFrame(output);
                       }
                   }).start();
    }


    protected int getResolutionWidthSetting() { return 320; }


    protected int getResolutionHeightSetting() { return 240; }


    protected int getFpsSetting() { return 30; }
    

    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here.
        // A default command runs without any driver action
        // Default Commands are not common for most subsystems
    }

}

