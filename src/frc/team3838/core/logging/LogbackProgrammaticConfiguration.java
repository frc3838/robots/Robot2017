package frc.team3838.core.logging;

import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.Status;
import ch.qos.logback.core.status.StatusManager;
import ch.qos.logback.core.status.WarnStatus;
import frc.team3838.core.Abstract3838Robot;
import frc.team3838.core.commands.common.SleepCommand;
import frc.team3838.core.commands.drive.AbstractDriveStraightCommand;
import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.game.Robot3838;
import frc.team3838.game.commands.drive.DrivePidRotateCommand;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.piCamera.PiCameraSubsystem;
import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import static ch.qos.logback.classic.Level.*;



/**
 * Because the roboRIO uses a compact2 JRE, we cannot use a standard logback XML file as the
 * XML based classes used to parse it are not available in a compact2 profile. We therefore
 * configure the logging programmatically. There is an outstanding to do to see if you can use
 * the groovy based logging configuration (i.e. logback.groovy) that Logback provides. But it
 * has not yet been explored (as this methodology works ok for our simple needs). For more
 * information on Java profiles, see:
 * <ol>
 *     <li><a href='https://blogs.oracle.com/jtc/entry/a_first_look_at_compact'>An Introduction to Java 8 Compact Profiles</a></li>
 *     <li><a href='http://www.oracle.com/technetwork/java/embedded/resources/tech/compact-profiles-overview-2157132.html'>Java SE Embedded 8 Compact Profiles Overview</a></li>
 * </ol>
 *
 *
 */
@SuppressWarnings({"UseOfSystemOutOrSystemErr", "UnusedDeclaration", "CallToPrintStackTrace", "ConstantConditions"})
public class LogbackProgrammaticConfiguration
{
    private static final Logger logger = LoggerFactory.getLogger(LogbackProgrammaticConfiguration.class);

    //@formatter:off
    /**
     * Boolean flag meant for use to guard Smart Dashboard Debug data or other Debugging output.
     * <strong>Setting this value does NOT activate DEBUG logging.</strong> Use {@link #rootLevel}
     * to change logging output level.
     */
    public static final boolean IN_DEBUGGING_MODE = false;

    //It's be nice if we could set this programmatically.
    public static final boolean IN_COMPETITION = false;

    /** The ROOT logger level. */
    public static final Level rootLevel = INFO;

    /** If logging framework should output initialization debug information. */
    public static final boolean logInitializationDebug = false;

    private static Builder<String, Level> levelSettingsMapBuilder = ImmutableMap.builder();

    // === SET INDIVIDUAL LOGGING LEVELS HERE ===
    static
    {

        // The first level is the level during the completion and should WARN or INFO
        // The second level is the level used during build season.

        set(LogTester.class, OFF, OFF);
        //Default all code to a default level. For competitions, this should be WARN
        setPackage("frc.team3838", WARN, INFO);


        // === PRIMARY CLASSES ===
        // We want the primary Robot class to be INFO for Competitions
        set(Abstract3838Robot.class, INFO, INFO);

        set(Robot3838.class, INFO, INFO);

        set(Subsystems.class, INFO, INFO);

        set(Abstract3838DriveTrainSubsystem.class, INFO, INFO);
        set(DriveTrainSubsystem.class, INFO, INFO);

        set(DrivePidRotateCommand.class, INFO, INFO);


        set(SleepCommand.class, INFO, INFO);


        set(AbstractDriveStraightCommand.class, WARN, INFO);

        setPackage(AbstractDriveStraightCommand.class, WARN, INFO);

        set(PiCameraSubsystem.class, WARN, INFO);
        setPackage(frc.team3838.game.commands.camera.pi.AbstractPiCameraCommand.class, WARN, INFO);



        // === LIBRARY CLASSES ===
        setPackage("org.reflections", WARN, WARN);















        // *** DO NOT PUT ANY LOG SETTINGS/CONFIGURATIONS BELOW THIS POINT **
        loggerLevels = levelSettingsMapBuilder.build();
        levelSettingsMapBuilder = null;
        // *** DO NOT PUT ANY LOG SETTINGS/CONFIGURATIONS AFTER THE INITIALIZATION OF loggerLevels THE SET **
    }























    //@formatter:on
    private static final ImmutableMap<String, Level> loggerLevels;

    /**
     * Flag that is automatically set based on the {@link #rootLevel} setting such that additional detail is
     * added to the logging output, namely the {@code caller} detail.
     */
    private static final boolean WITH_DETAIL = (rootLevel.equals(DEBUG) || rootLevel.equals(TRACE) || rootLevel.equals(ALL));


    //private static final String consolePatternLayoutBASE = "%date{HH:mm:ss.SSS zzz} %highlight(%-5level) [%-5thread] %-40logger{40} - %message";
    private static final String consolePatternLayoutBASE = "%date{HH:mm:ss.SSS zzz} %highlight(%-5level) %cyan(%-40logger{40}) %boldBlue(::) %message";
    private static final String consolePatternLayoutSUFFIX = WITH_DETAIL
                                                             ? "      \t\t==> %caller{1}"
                                                             : "%n";

    private static final String consolePatternLayout = consolePatternLayoutBASE + consolePatternLayoutSUFFIX;


    //Used when reporting status messages to indicate the origin of the message
    @SuppressWarnings("InstantiationOfUtilityClass")
    private static final LogbackProgrammaticConfiguration origin = new LogbackProgrammaticConfiguration();


    private LogbackProgrammaticConfiguration() { }


    /**
     * Sets the log level for a class. For example:
     * <pre>
     * set(CatapultSubSystem.class, WARN, INFO);
     * set(FireCatapultCommand.class, WARN, DEBUG);
     * </pre>
     * @param clazz the class to set the log level for
     * @param competitionLevel the log level to use during competition. In general, this should be WARN,
     *                         or possibly INFO as we want to reduce log verbosity during competition
     *
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     *                             are not being troubleshoot.
     */
    private static void set(@Nonnull Class<?> clazz, @Nonnull Level competitionLevel, @Nonnull Level developmentWorkLevel)
    {
        final Level level = IN_COMPETITION ? competitionLevel : developmentWorkLevel;
        levelSettingsMapBuilder.put(clazz.getName(), level);
    }


    /**
     * Sets the log level for a logger by name. This generally should be the fully qualified class name.
     * As such, it is preferable to use the {@link #set(Class, Level, Level)} method.
     * @param loggerName the name of the logger
     * @param competitionLevel the log level to use during competition. In general, this should be WARN,
     *                         or possibly INFO as we want to reduce log verbosity during competition
     *
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     *                             are not being troubleshoot.
     */
    private static void set(@Nonnull String loggerName, @Nonnull Level competitionLevel, @Nonnull Level developmentWorkLevel)
    {
        final Level level = IN_COMPETITION ? competitionLevel : developmentWorkLevel;
        levelSettingsMapBuilder.put(loggerName, level);
    }


    /**
     * Sets the log level for a package, and thus all classes in that package and ay of its sub-packages.
     * @param pkg the package to set the log level for.
     * @param competitionLevel the log level to use during competition. In general, this should be WARN,
     *                         or possibly INFO as we want to reduce log verbosity during competition
     *
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     *                             are not being troubleshoot.
     */
    private static void setPackage(@Nonnull Package pkg, @Nonnull Level competitionLevel, @Nonnull Level developmentWorkLevel)
    {
        final Level level = IN_COMPETITION ? competitionLevel : developmentWorkLevel;
        levelSettingsMapBuilder.put(pkg.getName(), level);
    }


    /**
     * Sets the log level for a package, and thus all classes in that package and ay of its sub-packages.
     * This package set will be the package of the provided class. This is done for convenience and is the
     * equivalent of {@code  setPackage(clazz.getPackage(), competitionLevel, developmentWorkLevel)}
     * @param clazz                a class in the package to set the log level for
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     *                             are not being troubleshoot.
     */
    private static void setPackage(@Nonnull Class<?> clazz, @Nonnull Level competitionLevel, @Nonnull Level developmentWorkLevel)
    {
        setPackage(clazz.getPackage(), competitionLevel, developmentWorkLevel);
    }

    /**
     * Sets the log level for a package by name, and thus all classes in that package and ay of its sub-packages.
     *
     * @param packageName          the name of the package to set the log level for
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     *                             are not being troubleshoot.
     */
    @SuppressWarnings("SameParameterValue")
    private static void setPackage(@Nonnull String packageName, @Nonnull Level competitionLevel, @Nonnull Level developmentWorkLevel)
    {
        set(packageName, competitionLevel, developmentWorkLevel);
    }

    public static void init()
    {
        try
        {
            final LoggerContext context = getLoggerContext();
            if (context != null)
            {
                reportInfo("Programmatically configuring logging");

                reportDebug("Resetting Context");
                context.reset();

                reportDebug("Creating Console Appender");
                ConsoleAppender<ILoggingEvent> consoleAppender = new ConsoleAppender<>();
                consoleAppender.setContext(context);
                consoleAppender.setName("CONSOLE");

                reportDebug("Creating Pattern Layout");
                PatternLayoutEncoder patternLayout = new PatternLayoutEncoder();
                patternLayout.setContext(context);
                patternLayout.setPattern(consolePatternLayout);
                reportDebug("Starting Pattern Layout");
                patternLayout.start();

                consoleAppender.setEncoder(patternLayout);
                reportDebug("Starting Console Appender");
                consoleAppender.start();

                reportDebug("Getting Root Logger");
                final ch.qos.logback.classic.Logger rootLogger = context.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
                rootLogger.addAppender(consoleAppender);
                reportDebug("Setting Root Logger Level to " + rootLevel);
                rootLogger.setLevel(rootLevel);

                reportDebug("Configuring various logging levels");
                if (loggerLevels != null)
                {
                    for (Entry<String, Level> entry : loggerLevels.entrySet())
                    {
                        final String loggerName = entry.getKey();
                        final Level level = entry.getValue();
                        context.getLogger(loggerName).setLevel(level);
                    }
                }
                reportInfo("Programmatic logging configuration has completed");
                reportInfo("Configuring System out and err over SLF4J");
                SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LogLevel.INFO, LogLevel.ERROR);

                //Give time for full initialization
                try {TimeUnit.MILLISECONDS.sleep(100);} catch (InterruptedException ignore) {}

                LogTester.log();
            }
            else
            {
                System.err.println(">>>>ERROR<<<< Could not Programmatically configuring logging since a reference to the LoggerContext could not be obtained.");
            }

        }
        catch (Exception e)
        {
            reportError(e);
        }

    }


    /**
     * Reports/logs an error message, via the Logback Context StatusManager to the console at
     * an 'error' status level.
     *
     * @param msg the message to log
     */
    protected static void reportError(@Nonnull String msg)
    {
        reportError(msg, null);
    }


    /**
     * Reports/logs an exception, via the Logback Context StatusManager to the console at
     * an 'error' status level. A default message, indicating an exception occurred, is used when
     * reporting the exception.
     *
     * @param t the throwable/exception to report
     */
    protected static void reportError(@Nonnull Throwable t)
    {
        reportError("An exception occurred during programmatic logging configuration via the " + LogbackProgrammaticConfiguration.class.getName(), t);
    }


    /**
     * Reports/logs an error message and an exception, via the Logback Context StatusManager
     * to the console at an 'error' status level.
     *
     * @param msg the message to log
     * @param t   the throwable/exception to report; if {@code null} only the message is logged
     */
    protected static void reportError(@Nonnull String msg, @Nullable Throwable t)
    {
        final StringBuilder fullMessage = new StringBuilder("LOGGING CONFIGURATION ERROR: ").append(msg);
        if (t != null)
        {
            fullMessage.append(" :: Exception: ").append(t.toString());
        }
        System.err.println(fullMessage.toString());
        reportStatus(new ErrorStatus(fullMessage.toString(), origin, t));
    }


    /**
     * Reports/logs a warning message, via the Logback Context StatusManager to the console at
     * a 'warn' status level.
     *
     * @param msg the message to log
     */
    protected static void reportWarning(@Nonnull String msg)
    {
        reportWarning(msg, null);
    }


    /**
     * Reports/logs an exception, via the Logback Context StatusManager to the console at
     * a 'warn' status level. A default message, indicating an exception occurred, is used when
     * reporting the exception.
     *
     * @param t the throwable/exception to report
     */
    protected static void reportWarning(@Nonnull Throwable t)
    {
        reportWarning("An exception occurred during programmatic logging configuration via the " + LogbackProgrammaticConfiguration.class.getName(), t);
    }


    /**
     * Reports/logs an warning message and an exception, via the Logback Context StatusManager
     * to the console at the 'warn' status level.
     *
     * @param msg the message to log
     * @param t   the throwable/exception to report; if {@code null} only the message is logged
     */
    protected static void reportWarning(@Nonnull String msg, @Nullable Throwable t)
    {
        final StringBuilder fullMessage = new StringBuilder("LOGGING CONFIGURATION WARNING: ").append(msg);
        if (t != null)
        {
            fullMessage.append(" :: Exception: ").append(t.toString());
        }
        System.out.println(fullMessage);

        reportStatus(new WarnStatus(fullMessage.toString(), origin, t));
    }


    /**
     * Reports/logs an informational message, via the Logback Context StatusManager to the console at
     * an 'info' status level.
     *
     * @param msg the message to log
     */
    protected static void reportInfo(@Nonnull String msg)
    {
        final String fullMessage = "LOGGING CONFIGURATION INFORMATION: " + msg;
        System.out.println(fullMessage);
        reportStatus(new InfoStatus(fullMessage, origin));
    }


    /**
     * Reports/logs a debug message, via the Logback Context StatusManager to the console at
     * an 'debug' status level.
     *
     * @param msg the message to log
     */
    protected static void reportDebug(@Nonnull String msg)
    {
        if (logInitializationDebug)
        {
            final String fullMessage = "LOGGING CONFIGURATION DEBUG INFO:  " + msg;
            System.out.println(fullMessage);
            reportStatus(new InfoStatus(fullMessage, origin));
        }
    }


    /**
     * An internal method used for reporting statuses.
     *
     * @param status the status to report
     */
    protected static void reportStatus(Status status)
    {
        try
        {
            final LoggerContext context = getLoggerContext();
            if (context != null)
            {
                StatusManager sm = context.getStatusManager();
                if (sm != null)
                {
                    sm.add(status);
                }
            }
        }
        catch (Exception e)
        {
            System.err.println("An exception occurred when attempting to report/log a Logback status. Cause details: " + e);
            e.printStackTrace();
        }
    }


    @Nullable
    protected static LoggerContext getLoggerContext()
    {
        try
        {
            // assume SLF4J is bound to logback in the current environment
            return (LoggerContext) LoggerFactory.getILoggerFactory();
        }
        catch (Exception e)
        {
            System.err.println("\">>>>ERROR<<<< Could not get LoggerContext reference. Cause Details: " + e.toString());
            e.printStackTrace();
            return null;
        }
    }
}
