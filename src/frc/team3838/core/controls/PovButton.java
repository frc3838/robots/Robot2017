package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Button;



public class PovButton extends Button
{
    private static final Logger logger = LoggerFactory.getLogger(PovButton.class);

    private final GenericHID hid;

    private final int activationAngle;


    public PovButton(GenericHID hid, int activationAngle)
    {
        this.hid = hid;
        this.activationAngle = activationAngle;
    }

    public PovButton(GenericHID hid, PovDirection povDirection)
    {
        this.hid = hid;
        this.activationAngle = povDirection.getAngle();
    }


    @Override
    public boolean get()
    {
        return hid.getPOV() == activationAngle;
    }


}
