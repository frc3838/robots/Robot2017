package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.buttons.Button;
import frc.team3838.core.meta.API;



@API
public class NoOpButton extends Button
{
    @Override
    public boolean get() { return false; }
}
