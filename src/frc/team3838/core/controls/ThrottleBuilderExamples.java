package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Joystick;



@SuppressWarnings("unused")
class ThrottleBuilderExamples
{
    private static final Logger logger = LoggerFactory.getLogger(ThrottleBuilderExamples.class);


    public void examples()
    {
        final Joystick joystick = new Joystick(99);

        // Build a throttle that uses the full range, of -1 to 1, of the Y axis of a joystick. Do not invert the output.
        final Throttle throttle = ThrottleBuilder.getBuilder()
                                                 .using(joystick)
                                                 .usingYAxis()
                                                 .usingBiDirectionality()
                                                 .usingNonInvertedOutput()
                                                 .withoutDeadZone()
                                                 .withoutScaling()
                                                 .withoutActivationButton()
                                                 .build();


        // Build a throttle that uses the negative (i.e. the value when pushing the joystick up)
        // range of 0 to -1 of the Y axis of a joystick. Do not invert the output.
        final Throttle throttle2 = ThrottleBuilder.getBuilder()
                                                  .using(joystick)
                                                  .usingYAxis()
                                                  .usingUpPushNegativeDirection()
                                                  .outputNegativeValue()
                                                  .withDeadZoneThreshold(0.01)
                                                  .withAbsoluteScaling(0.1, 0.6)
                                                  .withoutActivationButton()
                                                  .build();


        // In this example,
        //      1) we want to push the joystick forward to turn on a motor.
        //      2) we only want the motor to run in one direction.
        //      3) the motor takes a positive value (0 to 1)
        // Since pushing the joystick forward gives us a negative Y axis value, we need to
        // invert the value. We do this by using the outputPositiveValue() method
        final Throttle throttle3 = ThrottleBuilder.getBuilder()
                                                  .using(joystick)
                                                  .usingYAxis()
                                                  .usingUpPushNegativeDirection()
                                                  .outputPositiveValue()
                                                  .withDeadZoneThreshold(0.01)
                                                  .withAbsoluteScaling(0.1, 0.6)
                                                  .withoutActivationButton()
                                                  .build();
    }

}
