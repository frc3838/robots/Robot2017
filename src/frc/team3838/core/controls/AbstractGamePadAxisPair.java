package frc.team3838.core.controls;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;
import frc.team3838.core.utils.HidUtils;



@API
public abstract class AbstractGamePadAxisPair implements AxisPair
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Nonnull
    protected final GenericHID hid;


    @SuppressWarnings("ConstantConditions")
    protected AbstractGamePadAxisPair(@Nonnull GenericHID hid)
    {
        this.hid = hid;
        if (!HidUtils.isGamepad(hid))
        {

            logger.warn("A non gamepad HID was used to create a {}. Hid Type: {}  Hid Port: {}",
                        getClass().getSimpleName(),
                        hid != null ? hid.getClass().getSimpleName() : "<null>",
                        hid != null ? hid.getPort() : -1);
        }
    }
}
