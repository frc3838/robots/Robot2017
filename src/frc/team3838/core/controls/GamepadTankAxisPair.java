package frc.team3838.core.controls;

import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



@API
public class GamepadTankAxisPair extends AbstractGamePadAxisPair
{
    private final  int leftAxis;
    private final int rightAxis;


    public GamepadTankAxisPair(GenericHID hid)
    {
        this(hid, 1, 5);
    }

    public GamepadTankAxisPair(@Nonnull GenericHID hid, int leftAxis, int rightAxis)
    {
        super(hid);
        this.leftAxis = leftAxis;
        this.rightAxis = rightAxis;
    }

    @Override
    public double getXorLeft()
    {
        return hid.getRawAxis(leftAxis);
    }


    @Override
    public double getYorRight()
    {
        return hid.getRawAxis(rightAxis);
    }
}
