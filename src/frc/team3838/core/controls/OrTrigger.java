package frc.team3838.core.controls;

import java.util.Collection;
import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.meta.API;



@API
public class OrTrigger extends AbstractLogicTrigger
{
    @API
    public OrTrigger(@Nonnull Trigger trigger) { super(trigger); }


    @API
    public OrTrigger(@Nonnull Trigger trigger, Trigger... otherTriggers) { super(trigger, otherTriggers); }


    @API
    public OrTrigger(@Nonnull Collection<Trigger> triggers) { super(triggers); }


    @Override
    public boolean get()
    {
        for (Trigger trigger : triggers)
        {
            if (trigger.get())
            {
                return true;
            }
        }
        return false;
    }
}
