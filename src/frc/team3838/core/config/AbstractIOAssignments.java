package frc.team3838.core.config;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import frc.team3838.core.utils.ReflectionUtils3838;



public abstract class AbstractIOAssignments
{
    /** Static logger for use in this Abstract class. */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractIOAssignments.class);

    /** Logger for use in this class in non-static contexts and by implementing classes. */
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private static AtomicBoolean initializationCompleted = new AtomicBoolean(false);
    
   
    static 
    {
        if (!initializationCompleted.getAndSet(true))
        {
            boolean foundDuplicate = false;
            try
            {
                final Set<Class<? extends AbstractIOAssignments>> instances = ReflectionUtils3838.findImplementations(AbstractIOAssignments.class);

                


                for (Class<? extends AbstractIOAssignments> clazz : instances)
                {
                    final Multimap<Integer, String> duplicateCheckMultiMap = ArrayListMultimap.create();
                    String name = clazz.getSimpleName();
                    if (name.endsWith("s")) { name = name.substring(0, name.length() - 1); }
                    LOG.info("Checking {} assignments for duplicates", name);


                    // 1) Put all fields for this class into the multimap
                    final Field[] fields = clazz.getFields();
                    for (Field field : fields)
                    {
                        final String fieldName = field.getName();

                        if (int.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(fieldName))
                        {
                            final int port = field.getInt(null);
                            duplicateCheckMultiMap.put(port, fieldName);
                        }
                        else if (long.class.isAssignableFrom(field.getType()) && !"MXP_ADD_FACTOR".equals(fieldName))
                        {
                            final long port = field.getLong(null);
                            duplicateCheckMultiMap.put((int) port, fieldName);
                        }
                    }

                    // 2) Check for any duplicates...
                    List<Integer> ports = new ArrayList<>(duplicateCheckMultiMap.keySet());
                    Collections.sort(ports);

                    for (Integer port : ports)
                    {
                        final List<String> assignments = new ArrayList<>(duplicateCheckMultiMap.get(port));
                        if (assignments.size() != 1)
                        {
                            foundDuplicate = true;
                            final String msg = String.format(">>>>>FATAL ERROR : %s port/channel # %s is duplicated! It is assigned to: '%s'.",
                                                             name,
                                                             port,
                                                             assignments);
                            LOG.error(msg);
                        }
                    }
                }

                // If you found any duplicates for any of the assignment subclasses, it is a fatal condition, so we throw na error
                if (foundDuplicate)
                {
                    
                    LOG.error("");
                    LOG.error(">>>>>FATAL ERROR<<<<<");
                    LOG.error(">>>>>FATAL ERROR: Duplicate port/channel assignments found as listed above.");
                    LOG.error(">>>>>FATAL ERROR<<<<<");
                    LOG.error("");
                    // Give the logging framework time to flush
                    try {TimeUnit.MILLISECONDS.sleep(250);} catch (InterruptedException ignore) {}
                    throw new ExceptionInInitializerError("Duplicate port/channel assignments were found. See log output for details.");
                }

            }
            catch (Exception e)
            {
                final String msg = String.format("Could not validate port/channel assignments due to an Exception. Cause Details: %s", e.toString());
                LOG.warn(msg, e);
                throw new RuntimeException(msg, e);
            }
        }
    }
    
    public static void initAllPortAndChannelAssignments()
    {
       /* a no op convene method to have the static initializer invoke */
    }
}
