package frc.team3838.core.commands;

import java.util.Set;
import javax.annotation.Nonnull;

import frc.team3838.core.subsystems.I3838Subsystem;



public abstract class Abstract3838NoSubsystemsCommand extends Abstract3838Command
{

    protected Abstract3838NoSubsystemsCommand()
    {
    }


    protected Abstract3838NoSubsystemsCommand(String name)
    {
        super(name);
    }


    protected Abstract3838NoSubsystemsCommand(double timeout)
    {
        super(timeout);
    }


    protected Abstract3838NoSubsystemsCommand(String name, double timeout)
    {
        super(name, timeout);
    }


    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems() { return NO_SUBSYSTEMS; }


    @Override
    protected void initializeImpl() throws Exception { /* No op */ }


    @Override
    protected boolean isFinishedImpl() throws Exception { return true; }


    @Override
    protected void endImpl() throws Exception { /* no op */ }

}
