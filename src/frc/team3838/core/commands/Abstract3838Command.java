package frc.team3838.core.commands;

import java.util.Set;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.core.meta.DoesNotThrowExceptions;
import frc.team3838.core.subsystems.I3838Subsystem;



public abstract class Abstract3838Command extends Command
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @SuppressWarnings("WeakerAccess")
    protected boolean hasHadException = false;

    @SuppressWarnings("WeakerAccess")
    protected boolean initializedSuccessfully = true;

    private long lastDisabledLogTime = 0;

    protected static final Set<I3838Subsystem> NO_SUBSYSTEMS = ImmutableSet.of();

    private String disabledMessage = String.format("Not all required subsystems for the command '%s' are enabled, or there was a problem when "
                                                   + "initializing the command. Therefore the command can not be and will not be executed.",
                                                   getClass().getSimpleName());


    @SuppressWarnings("unused")
    protected Abstract3838Command()
    {
        super();
        registerRequiredSubsystems();
    }


    @SuppressWarnings("unused")
    protected Abstract3838Command(String name)
    {
        super(name);
        registerRequiredSubsystems();
    }


    @SuppressWarnings("unused")
    protected Abstract3838Command(double timeout)
    {
        super(timeout);
        registerRequiredSubsystems();
    }


    @SuppressWarnings("unused")
    protected Abstract3838Command(String name, double timeout)
    {
        super(name, timeout);
        registerRequiredSubsystems();
    }


    @Nonnull
    protected abstract Set<I3838Subsystem> getRequiredSubsystems();

    protected abstract void initializeImpl() throws Exception;

    protected abstract void executeImpl() throws Exception;

    protected abstract boolean isFinishedImpl() throws Exception;

    protected abstract void endImpl() throws Exception;


    protected void interruptedImpl() throws Exception
    {
        end();
    }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @DoesNotThrowExceptions
    @Override
    protected final void initialize()
    {
        hasHadException = false;
        initializedSuccessfully = false;
        disabledMessage = String.format("Not all required subsystems for the command '%s' are enabled, or there was a problem when "
                                        + "initializing the command. Therefore the command can not be and will not be executed.",
                                        getName());

        if (areAllSubsystemsAreEnabled())
        {
            logger.debug("{}.initialize() called", getName());
            try
            {
                initializeImpl();
                initializedSuccessfully = true;
            }
            catch (Exception e)
            {
                hasHadException = true;
                logger.error("An exception occurred when initializing the command '{}'.", getName(), e);
            }
        }
        else
        {
            logger.warn("Not all required subsystems for the command '{}' are enabled. Therefore the command can not be and will not be initialized or executed.",
                        getName());
        }
    }


    @DoesNotThrowExceptions
    @Override
    protected final void execute()
    {
        super.execute();
        if (areAllSubsystemsAreEnabled())
        {
            try
            {
                executeImpl();
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getName(), e);
                hasHadException = true;
            }
        }
        else
        {
            if (lastDisabledLogTime == 0)
            {
                logger.debug("In execute: {}", disabledMessage);
                lastDisabledLogTime = System.currentTimeMillis();
            }
            else if (logger.isTraceEnabled())
            {
                final long currentTime = System.currentTimeMillis();
                if (currentTime > lastDisabledLogTime + 10_000)
                {
                    logger.trace("In execute: {}", disabledMessage);
                    lastDisabledLogTime = currentTime;
                }
            }
        }
    }


    @DoesNotThrowExceptions
    @Override
    protected final boolean isFinished()
    {
        try
        {
            return !initializedSuccessfully || hasHadException || isFinishedImpl();
        }
        catch (Exception e)
        {
            logger.error("An exception occurred when calling {}.isFinished(). Cause Summary: {}", getName(), e.toString(), e);
            hasHadException = true;
            return true;
        }
    }


    @DoesNotThrowExceptions
    @Override
    protected final void end()
    {
        // Called once after isFinished returns true
        // do any clean up or post command work here
        try
        {
            endImpl();
        }
        catch (Exception e)
        {
            logger.error("An exception occurred when calling {}.end(). Cause Summary: {}", getName(), e.toString(), e);
        }
    }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @DoesNotThrowExceptions
    @Override
    protected final void interrupted()
    {
        try
        {
            interruptedImpl();
        }
        catch (Exception e)
        {
            logger.error("An exception occurred when calling {}.end(). Cause Summary: {}", getName(), e.toString(), e);
            hasHadException = true;
        }
    }


    private void registerRequiredSubsystems()
    {
        for (I3838Subsystem subsystem : getRequiredSubsystems())
        {
            if (subsystem != null)
            {
                requires((Subsystem) subsystem);
            }
        }
    }


    protected boolean areAllSubsystemsAreEnabled()
    {
        for (I3838Subsystem subsystem : getRequiredSubsystems())
        {
            if (!subsystem.isEnabled())
            {
                return false;
            }
        }
        //All subsystems are enabled. So as long as we are initialized properly, we are good to go.

        //TODO - Need to troubleshot the original code - initializedSuccessfully was always false
        return true; // initializedSuccessfully && !hasHadException;
    }


}
