package frc.team3838.core.commands.drive;

import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.core.utils.MathUtils;



public abstract class AbstractDriveForwardSetDistanceCommand extends AbstractDriveStraightCommand
{
    protected final double targetDistanceInInches;
    protected double endDistanceInInches;

    protected double lastLogSpeed = -1;


    protected AbstractDriveForwardSetDistanceCommand(double targetDistanceInInches)
    {
        super(RobotDirection.Forward, defaultSpeedSetting);
        this.targetDistanceInInches = targetDistanceInInches;
    }

    protected AbstractDriveForwardSetDistanceCommand(double targetDistanceInInches, boolean enableTuning)
    {
        super(RobotDirection.Forward, defaultSpeedSetting, enableTuning);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    /**
     * The initialize method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute
     * command is called repeated until isFinished returns true,
     * or interrupted is called by the command scheduler/runner.
     * After isFinished returns true, end is called one time
     * in order to do any cleanup or set and values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {
        getDriveTrainSubsystem().resetEncoders();
        endDistanceInInches = getDriveTrainSubsystem().getAverageDistanceInInches() + targetDistanceInInches;
        logger.info("DriveForwardSetDistance with an end distance of {}", endDistanceInInches);
        super.initializeImpl();
    }


    /**
     * The execute method is called repeatedly when this Command is
     * scheduled to run until. It is called repeatedly until
     * the either isFinish returns true, interrupted is called,
     * or the command is canceled. Note that the initialize
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected void executeImpl() throws Exception
    {
        final double remainingDistance = calculateRemainingDistance();

        //SmartDashboard.putString("Remaining Distance", MathUtils.formatNumber(remainingDistance, 3));

        if (remainingDistance > 24)
        {
            speed = getMaxSpeed();
        }
        else if (remainingDistance > 18)
        {
            speed = getMaxSpeed() * 0.75;
        }
        else if (remainingDistance > 12)
        {
            speed = getMaxSpeed() * 0.50;
        }
        else if (remainingDistance > 8)
        {
            speed = getMaxSpeed() * 0.25;
        }
        else if (remainingDistance < 3)
        {
            speed = getMaxSpeed() * 0.10;
        }
        else if (remainingDistance <= 0)
        {
            speed = 0;
        }

        if (speed > 0 && speed < getMinSpeed())
        {
            speed = getMinSpeed();
        }

        if (logger.isInfoEnabled() && (lastLogSpeed != speed))
        {
            logger.info("Speed set to {}%", MathUtils.formatNumber(speed, 3));
            lastLogSpeed = speed;
        }
        driveStraight();
}


    private double calculateRemainingDistance()
    {
        return endDistanceInInches - getDriveTrainSubsystem().getAverageDistanceInInches();
    }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected boolean shouldApplyCorrection()
    {
        final double remainingDistance = calculateRemainingDistance();
        // Don't correct for the first 1/2 inch
        return !(remainingDistance < 0.5);
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return getDriveTrainSubsystem().getAverageDistanceInInches() >= endDistanceInInches;
    }


    protected abstract double getMaxSpeed();
    protected abstract double getMinSpeed();
}
