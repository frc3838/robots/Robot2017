package frc.team3838.core.commands.drive;

import javax.annotation.Nonnull;

import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;



public abstract class AbstractDriveStraightForTimePeriodCommand extends AbstractDriveStraightCommand
{
    private double runTimeInMs = 0;
    private double startTime;


    /**
     * Creates a drive straight command that runs in the forward direction at the default speed
     * for the specified time.
     *
     * @param runTimeInMilliseconds the time to run in Milliseconds.
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(double runTimeInMilliseconds)
    {
        super(RobotDirection.Forward, defaultSpeedSetting);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the default speed
     * for the specified time.
     *
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param enableTuning          if tuning of the command should be enabled (during development and testing)
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(double runTimeInMilliseconds, boolean enableTuning)
    {
        super(RobotDirection.Forward, defaultSpeedSetting, enableTuning);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the specified speed
     * for the specified time.
     *
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed)
    {
        super(RobotDirection.Forward, speed);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the specified speed
     * for the specified time.
     *
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     * @param enableTuning          if tuning of the command should be enabled (during development and testing)
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed, boolean enableTuning)
    {
        super(RobotDirection.Forward, speed, enableTuning);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the specified direction at the specified speed
     * for the specified time.
     *
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     * @param robotDirection        the direction to run in
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection)
    {
        super(robotDirection, speed);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the specified direction at the specified speed
     * for the specified time.
     *
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     * @param robotDirection        the direction to run in
     * @param enableTuning          if tuning of the command should be enabled (during development and testing)
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection, boolean enableTuning)
    {
        super(robotDirection, speed, enableTuning);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the default speed
     * for the specified time.
     *
     * @param name                  the command name
     * @param runTimeInMilliseconds the time to run in Milliseconds.
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds)
    {
        super(name, RobotDirection.Forward, defaultSpeedSetting);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the default speed
     * for the specified time.
     *
     * @param name                  the command name
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param enableTuning          if tuning of the command should be enabled (during development and testing)
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, boolean enableTuning)
    {
        super(name, RobotDirection.Forward, defaultSpeedSetting, enableTuning);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the specified speed
     * for the specified time.
     *
     * @param name                  the command name
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed)
    {
        super(name, RobotDirection.Forward, speed);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the forward direction at the specified speed
     * for the specified time.
     *
     * @param name                  the command name
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     * @param enableTuning          if tuning of the command should be enabled (during development and testing)
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed, boolean enableTuning)
    {
        super(name, RobotDirection.Forward, speed, enableTuning);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the specified direction at the specified speed
     * for the specified time.
     *
     * @param name                  the command name
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     * @param robotDirection        the direction to run in
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(@Nonnull String name, double runTimeInMilliseconds, double speed, @Nonnull RobotDirection robotDirection)
    {
        super(robotDirection, speed);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    /**
     * Creates a drive straight command that runs in the specified direction at the specified speed
     * for the specified time.
     *
     * @param name                  the command name
     * @param runTimeInMilliseconds the time to run for in Milliseconds
     * @param speed                 the speed to run at
     * @param robotDirection        the direction to run in
     * @param enableTuning          if tuning of the command should be enabled (during development and testing)
     */
    @API
    protected AbstractDriveStraightForTimePeriodCommand(@Nonnull String name,
                                                        double runTimeInMilliseconds,
                                                        double speed,
                                                        @Nonnull RobotDirection robotDirection,
                                                        boolean enableTuning)
    {
        super(robotDirection, speed, enableTuning);
        this.runTimeInMs = runTimeInMilliseconds;
    }


    @Override
    protected void initializeImpl() throws Exception
    {
        startTime = System.currentTimeMillis();
        super.initializeImpl();
    }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return System.currentTimeMillis() > (startTime + runTimeInMs);
    }


    @Nonnull
    @Override
    protected abstract Abstract3838DriveTrainSubsystem getDriveTrainSubsystem();

    @Nonnull
    @Override
    protected abstract Abstract3838NavxSubsystem getNavxSubsystem();
}
