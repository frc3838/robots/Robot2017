package frc.team3838.core.commands.common;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;



public class SleepCommand extends Abstract3838NoSubsystemsCommand
{
    private long startTime;
    private long delay;
    private long diff;

    private long lastTraceLogTime = 0;

    @Nullable
    private String dashboardKey = null;

    private long dashboardDefault = -1;


    //TODO: add variations in which a name can be specified

    @SuppressWarnings("unused")
    public static SleepCommand createSeconds(long seconds)
    {
        return new SleepCommand(TimeUnit.SECONDS.toMillis(seconds));
    }


    @SuppressWarnings("unused")
    public static SleepCommand createSeconds(long seconds, PartialSeconds partialSeconds)
    {
        return new SleepCommand(TimeUnit.SECONDS.toMillis(seconds) + partialSeconds.toMills());
    }


    @SuppressWarnings("unused")
    public static SleepCommand createSeconds(long seconds, long additionalMilliseconds)
    {
        return new SleepCommand(TimeUnit.SECONDS.toMillis(seconds) + additionalMilliseconds);
    }


    @SuppressWarnings("unused")
    public static SleepCommand create(long duration, TimeUnit timeUnit)
    {
        return new SleepCommand(timeUnit.toMillis(duration));
    }


    @SuppressWarnings("unused")
    public static SleepCommand create(long duration1, TimeUnit timeUnit1, long duration2, TimeUnit timeUnit2)
    {
        long totalDurationInMs = timeUnit1.toMillis(duration1) + timeUnit2.toMillis(duration2);
        return new SleepCommand(totalDurationInMs);
    }


    @SuppressWarnings("unused")
    public static SleepCommand create(long duration1, TimeUnit timeUnit1, long duration2, TimeUnit timeUnit2, long duration3, TimeUnit timeUnit3)
    {
        long totalDurationInMs = timeUnit1.toMillis(duration1) + timeUnit2.toMillis(duration2) + timeUnit3.toMillis(duration3);
        return new SleepCommand(totalDurationInMs);
    }


    @SuppressWarnings("unused")
    public static SleepCommand createReadFromDashboard(@Nonnull String dashboardKey, long defaultValueInSeconds)
    {
        return new SleepCommand(dashboardKey, defaultValueInSeconds);
    }


    private SleepCommand(long milliseconds)
    {
        this(milliseconds, "SleepCommand_" + milliseconds + "ms");
    }

    private SleepCommand(long milliseconds, String name)
    {
        super(name);
        delay = milliseconds;
        logger.debug("{} created for {}ms", name, delay);
    }


    private SleepCommand(@Nonnull String dashboardKey, long defaultValueInSeconds)
    {
        this("SleepCommand_SmartDashBoard", dashboardKey, defaultValueInSeconds);
    }

    private SleepCommand(@Nonnull String name, @Nonnull String dashboardKey, long defaultValueInSeconds)
    {
        super(name);
        this.dashboardKey = dashboardKey;
        this.dashboardDefault = defaultValueInSeconds;
        final double currentValue = SmartDashboard.getNumber(dashboardKey, -100);
        if (currentValue == -100)
        {
            //It's not on the dashboard yet.. so place it with the default value
            SmartDashboard.putNumber(dashboardKey, defaultValueInSeconds);
        }
        logger.debug("SleepCommand with time (in seconds) as read by SmartDashboard key {} with default time of {}", dashboardKey, defaultValueInSeconds);
    }


    @Override
    protected void initializeImpl() throws Exception
    {
        super.initializeImpl();
        if (dashboardKey != null)
        {
            try
            {
                final long seconds = (long) SmartDashboard.getNumber(dashboardKey, dashboardDefault);
                delay = TimeUnit.SECONDS.toMillis(seconds);
                logger.debug("Using delay of {} seconds /  {}ms as read from the dashboard", seconds, delay);
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred when reading delay time from SmartDashboard. Using default delay value of {}", dashboardDefault);
                delay = dashboardDefault;
            }
        }
        startTime = System.currentTimeMillis();
        logger.debug("starting {} for {}ms at {}  (startTime = {})", getName(), delay, LocalTime.now(), startTime);
    }


    protected void executeImpl()
    {
        final long currentTime = System.currentTimeMillis();
        diff = currentTime - startTime;

        if (logger.isTraceEnabled() && currentTime > lastTraceLogTime + 250)
        {
            logger.trace("    In sleep execute for {}, diff = {}", getName(), diff);
            lastTraceLogTime = currentTime;
        }
    }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    protected boolean isFinishedImpl()
    {
        return diff >= delay;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void endImpl()
    {
        logger.debug("Ending   {} of  {} ms at {}", getName(), delay, LocalTime.now());
    }


    @SuppressWarnings("unused")
    public enum PartialSeconds
    {
        NoPartial
            {
                @Override
                long toMills()
                {
                    return 0;
                }
            },
        OneSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 63;
                }
            },
        OneEighthSecond
            {
                @Override
                long toMills()
                {
                    return 125;
                }
            },
        ThreeSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 188;
                }
            },
        OneQuarterSecond
            {
                @Override
                long toMills()
                {
                    return 250;
                }
            },
        FiveSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 313;
                }
            },
        ThreeEightsSecond
            {
                @Override
                long toMills()
                {
                    return 375;
                }
            },
        SevenSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 438;
                }
        },
        HalfSecond
            {
                @Override
                long toMills()
                {
                    return 500;
                }
            },
        NineSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 563;
                }
            },
        FiveEightsSecond
            {
                @Override
                long toMills()
                {
                    return 625;
                }
            },
        ElevenSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 688;
                }
            },
        ThreeQuartersSecond
            {
                @Override
                long toMills()
                {
                    return 750;
                }
            },
        ThirteenSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 813;
                }
            },
        SevenEightsSecond
            {
                @Override
                long toMills()
                {
                    return 875;
                }
            },
        FifteenSixteenthSecond
            {
                @Override
                long toMills()
                {
                    return 938;
                }
            };


        abstract long toMills();
    }
}
