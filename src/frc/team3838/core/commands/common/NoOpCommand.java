package frc.team3838.core.commands.common;

import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;


// @formatter:off
public final class NoOpCommand extends Abstract3838NoSubsystemsCommand
{
    private static final NoOpCommand INSTANCE = new NoOpCommand();

    public static NoOpCommand getInstance() {return INSTANCE; }
    
    private NoOpCommand() { }
    
    @Override protected void executeImpl() { /*no op*/ }
}
// @formatter:on
