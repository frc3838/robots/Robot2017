package frc.team3838.core.commands.log;

import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;



/**
 * Simple command that logs a statement that the next step in an autonomous sequence is starting.
 * Typically this is for use when "recording", via logging, the driver's actions when determining 
 * actions needed to mimic a driver's actions.
 */
public class NextStepLogStatement extends Abstract3838NoSubsystemsCommand
{
    @Override
    protected void executeImpl() throws Exception
    {
        logger.info("================================================================================");
        logger.info("================================================================================");
        logger.info("  >>> NEXT STEP...");
        logger.info("================================================================================");
        logger.info("================================================================================");
    }
}
