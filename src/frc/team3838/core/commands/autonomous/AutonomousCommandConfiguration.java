package frc.team3838.core.commands.autonomous;

import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.command.Command;



public interface AutonomousCommandConfiguration
{
    @Nonnull
    Command getCommand();
    @Nonnull
    String getDescription();
}
