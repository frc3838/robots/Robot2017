package frc.team3838.core.commands.autonomous;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.meta.API;



@API
public class AutonomousCommandConfigurationImpl implements AutonomousCommandConfiguration
{
    @Nonnull
    private final Command command;
    @Nonnull
    private final String description;


    public AutonomousCommandConfigurationImpl(@Nonnull String description, @Nonnull Command command)
    {
        this.command = command;
        this.description = description;
    }


    @Nonnull
    @Override
    public Command getCommand()
    {
        return command;
    }


    @Nonnull
    @Override
    public String getDescription()
    {
        return description;
    }


    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("description", description)
            .toString();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AutonomousCommandConfigurationImpl that = (AutonomousCommandConfigurationImpl) o;

        return new EqualsBuilder()
            .append(command, that.command)
            .append(description, that.description)
            .isEquals();
    }


    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37)
            .append(command)
            .append(description)
            .toHashCode();
    }
}
