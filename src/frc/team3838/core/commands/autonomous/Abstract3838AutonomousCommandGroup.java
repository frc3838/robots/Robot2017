package frc.team3838.core.commands.autonomous;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.commands.Abstract3838CommandGroup;
import frc.team3838.core.commands.log.LogCommandGroupFinishedCommand;
import frc.team3838.core.commands.log.LogCommandGroupStartCommand;



@SuppressWarnings("AbstractClassExtendsConcreteClass")
public abstract class Abstract3838AutonomousCommandGroup extends Abstract3838CommandGroup
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final LogCommandGroupStartCommand logStartCommand;


    @SuppressWarnings("unused")
    protected Abstract3838AutonomousCommandGroup()
    {
        super();
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        addCommands();
        postCommands();
    }


    @SuppressWarnings("unused")
    protected Abstract3838AutonomousCommandGroup(String name)
    {
        super(name);
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        addCommands();
        postCommands();
    }


    @SuppressWarnings("unused")
    protected Abstract3838AutonomousCommandGroup(List<Command> sequentialCommands)
    {
        super();
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        sequentialCommands.forEach(this::addSequential);
        addCommands();
        postCommands();
    }


    @SuppressWarnings("unused")
    protected Abstract3838AutonomousCommandGroup(String name, List<Command> sequentialCommands)
    {
        super(name);
        logStartCommand = new LogCommandGroupStartCommand(this);
        preCommands();
        sequentialCommands.forEach(this::addSequential);
        addCommands();
        postCommands();
    }





    /**
     * Add commands in this method:
     * <pre>
     *      Run in Sequence
     *          e.g. addSequential(new Command1());
     *               addSequential(new Command2());
     *      these will run in order.
     *
     *      To run multiple commands at the same time, use addParallel()
     *          e.g. addParallel(new Command1());
     *               addSequential(new Command2());
     *      Command1 and Command2 will run in parallel.
     *
     *      A command group will require all of the subsystems that each member would require.
     *          e.g. if Command1 requires chassis, and Command2 requires arm,
     *          a CommandGroup containing them would require both the chassis and the arm.
     * </pre>
     */
    protected abstract void addCommands();



    protected void preCommands()
    {
        addSequential(logStartCommand);
        addSequential(new AutonomousDisableMotorSafetyCommand());
    }


    protected void postCommands()
    {
        addSequential(new LogCommandGroupFinishedCommand(this, logStartCommand));
        addSequential(new AutonomousDisableMotorSafetyCommand());
    }


}
