package frc.team3838.core.commands;

import java.util.Set;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.Subsystems;



public abstract class AbstractBasicDriveCommand extends Abstract3838Command
{
    protected  final I3838DriveTrainSubsystem driveTrainSubsystem;


    protected AbstractBasicDriveCommand()
    {
        this.driveTrainSubsystem = Subsystems.getDriveTrainSubsystem();
    }


    protected AbstractBasicDriveCommand(I3838DriveTrainSubsystem driveTrainSubsystem)
    {
        this.driveTrainSubsystem = driveTrainSubsystem;
    }


    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        return driveTrainSubsystem != null ? ImmutableSet.of(driveTrainSubsystem) : NO_SUBSYSTEMS;
    }


    @Override
    protected boolean areAllSubsystemsAreEnabled()
    {
        return driveTrainSubsystem != null && driveTrainSubsystem.isEnabled();
    }
}
