package frc.team3838.core.hardware;

import javax.annotation.Nullable;

import com.ctre.CANTalon;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;


@API
public class CANTalonPair implements SpeedController
{
    private final CANTalon master;
    private final CANTalon slave;

    @API
    public CANTalonPair(int masterChannel, int slaveChannel)
    {
        master = createCanTalon(masterChannel);
        slave = createCanTalon(slaveChannel, masterChannel);
    }



    /**
     * Creates either a standalone/master CANTalon. To create a slave/following CANTalon, see
     * {@link #createCanTalon(int, Integer)},
     *
     * @param channel the CAN bus channel number
     *
     * @return the created CANTalon
     *
     * @see #createCanTalon(int, Integer)
     */
    private static CANTalon createCanTalon(int channel)
    {
        return createCanTalon(channel, null);
    }


    /**
     * Creates either a standalone/master CANTalon, or a CANTalon slaved to (i.e. following) another
     * talon (by specifying the master channel), If {@code channelOfMasterToSalveTo} is null,
     * a standalone/master talon is created. If {@code channelOfMasterToSalveTo} has a value,
     * a slave talon is created that follows, or is slaved to, the master specified by the
     * {@code channelOfMasterToSalveTo}.
     *
     * @param channel                  the CAN bus channel number
     * @param channelOfMasterToSalveTo If {@code channelOfMasterToSalveTo} is null,
     *                                 a standalone/master talon is created. If {@code channelOfMasterToSalveTo} has a value,
     *                                 a slave talon is created that follows, or is slaved to, the master specified by the
     *                                 {@code channelOfMasterToSalveTo}.
     *
     * @return the created CANTalon
     */
    private static CANTalon createCanTalon(int channel, @Nullable Integer channelOfMasterToSalveTo)
    {
        CANTalon canTalon = new CANTalon(channel);
        canTalon.enableBrakeMode(true);
        if (channelOfMasterToSalveTo == null)
        {
            canTalon.changeControlMode(TalonControlMode.PercentVbus);
        }
        else
        {
            canTalon.changeControlMode(TalonControlMode.Follower);
            canTalon.set(channelOfMasterToSalveTo); // The channel to follow
        }
        canTalon.enableControl();
        return canTalon;
    }

    @API
    public CANTalon getMaster()
    {
        return master;
    }

    @API
    public CANTalon getSlave()
    {
        return slave;
    }


    @Override
    public double get() {return master.get();}


    @Override
    public void set(double speed) {master.set(speed);}


    @Override
    public void setInverted(boolean isInverted) {master.setInverted(isInverted);}


    @Override
    public boolean getInverted() {return master.getInverted();}


    @Override
    public void disable() {master.disable();}


    @Override
    public void stopMotor()
    {
        //noinspection deprecation
        master.stopMotor();
    }


    @Override
    public void pidWrite(double output) {master.pidWrite(output);}
}
