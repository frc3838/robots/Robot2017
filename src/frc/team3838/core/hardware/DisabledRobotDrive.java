package frc.team3838.core.hardware;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;



/**
 * A {@link RobotDrive} implementation that does nothing. It can be used so {@code RobotDrive} variables./fields can be
 * guaranteed to not be null. Furthermore, it prevents "... Output not updated often enough" warnings by managing the
 * the motor safety watchdog.
 */
@API
public class DisabledRobotDrive extends RobotDrive
{
    public static final SpeedController noOpSpeedController = new NoOpSpeedController();

    private final ScheduledExecutorService scheduledExecutorService;

    public DisabledRobotDrive()
    {
        super(noOpSpeedController, noOpSpeedController);
        m_safetyHelper.setSafetyEnabled(false);
        m_safetyHelper.feed();

        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(() -> m_safetyHelper.feed(), 0, 20, TimeUnit.MILLISECONDS);
    }

    public SpeedController getSpeedController() { return DisabledRobotDrive.noOpSpeedController; }

}
