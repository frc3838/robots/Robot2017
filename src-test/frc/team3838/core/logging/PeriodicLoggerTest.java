package frc.team3838.core.logging;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.config.time.TimeSetting;



public class PeriodicLoggerTest
{
    private static ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);;
    private static final Logger logger = LoggerFactory.getLogger(PeriodicLoggerTest.class);


    public static void main(String[] args)
    {
        final PeriodicLogger logger5Sec = new PeriodicLogger(   logger, new TimeSetting("5 second"));
        final PeriodicLogger logger10Sec = new PeriodicLogger(  logger, new TimeSetting("10 second"));
        final PeriodicLogger logger2Sec = new PeriodicLogger(   logger, new TimeSetting("2 second"));
        final PeriodicLogger loggerHalfSec = new PeriodicLogger(logger, new TimeSetting("500 milliseconds"));
        final PeriodicLogger logger1Min = new PeriodicLogger(   logger, new TimeSetting("1 minute"));

        scheduledExecutorService.scheduleAtFixedRate(() ->
                                                     {
                                                         //logger.debug("DEBUG Log message at {}", DateTime.now());
                                                         final DateTime now = DateTime.now();
                                                         logger1Min.info(    "INFO  1 Minute logger Log message at {}", now);
                                                         logger10Sec.info(   "INFO 10 Second logger Log message at {}", now);
                                                         logger5Sec.info(    "INFO  5 Second logger Log message at {}", now);
//                                                         logger2Sec.info(    "INFO  2 Second logger Log message at {}", now);
//                                                         loggerHalfSec.info( "INFO  ½ Second logger Log message at {}", now);

                                                     }, 0, 20, TimeUnit.MILLISECONDS);
        
        try {TimeUnit.MINUTES.sleep(2);} catch (InterruptedException ignore) {}
    }
}