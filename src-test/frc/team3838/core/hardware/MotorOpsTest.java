package frc.team3838.core.hardware;

import edu.wpi.first.wpilibj.SpeedController;

import static org.junit.Assert.*;



@SuppressWarnings("Duplicates")
public class MotorOpsTest
{
    
    


    @org.junit.Test
    public void modifySpeedWithOutDirectionChange() throws Exception
    {
        SpeedController speedController = new MockSpeedController();
        MotorOps ops = new MotorOps(speedController, false);

        // Verify no direction change, from forward to stop
        ops.setSpeed(0.5);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#1: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);

        // Verify no direction change, from reverse to stop
        ops.setSpeed(-0.5);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#2: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);

        //Attempt to change speed from stopped        ops.setSpeed(0.0);
        ops.modifySpeedWithOutDirectionChange(0.7);
        assertEquals("#3: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);

        //From forward to increased forward, should be constrained
        ops.setSpeed(0.1);
        ops.modifySpeedWithOutDirectionChange(2.0);
        assertEquals("#4: Wrong MotorOps Speed Setting", 1.0, ops.getSpeed(), 0.002);

        //From reverse to increased reverse, should be constrained
        ops.setSpeed(-0.1);
        ops.modifySpeedWithOutDirectionChange(2.0);
        assertEquals("#5: Wrong MotorOps Speed Setting", -1.0, ops.getSpeed(), 0.002);

        //Forward speed increases
        ops.setSpeed(0.1);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#6a: Wrong MotorOps Speed Setting", 0.2, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#6b: Wrong MotorOps Speed Setting", 0.3, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.2);
        assertEquals("#6c: Wrong MotorOps Speed Setting", 0.5, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.7);
        assertEquals("#6d: Wrong MotorOps Speed Setting", 1.0, ops.getSpeed(), 0.002);

        //Forward speed decreases
        ops.setSpeed(1.0);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#7a: Wrong MotorOps Speed Setting", 0.9, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#7b: Wrong MotorOps Speed Setting", 0.8, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.2);
        assertEquals("#7c: Wrong MotorOps Speed Setting", 0.6, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#7d: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);

        //Reverse speed increases
        ops.setSpeed(-0.1);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#8a: Wrong MotorOps Speed Setting", -0.2, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#8b: Wrong MotorOps Speed Setting", -0.3, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.2);
        assertEquals("#8c: Wrong MotorOps Speed Setting", -0.5, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.7);
        assertEquals("#8d: Wrong MotorOps Speed Setting", -1.0, ops.getSpeed(), 0.002);


        //Reverse speed decreases
        ops.setSpeed(-1.0);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#9a: Wrong MotorOps Speed Setting", -0.9, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#9b: Wrong MotorOps Speed Setting", -0.8, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.2);
        assertEquals("#9c: Wrong MotorOps Speed Setting", -0.6, ops.getSpeed(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#9d: Wrong MotorOps Speed Setting", -0.0, ops.getSpeed(), 0.002);
    }


    @org.junit.Test
    public void modifySpeedWithOutDirectionChange_withInvertedMotor() throws Exception
    {
        SpeedController speedController = new MockSpeedController();
        MotorOps ops = new MotorOps(speedController, true);

        // Verify no direction change, from forward to stop
        ops.setSpeed(0.5);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#1: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);
        assertEquals("#1: Wrong Speed Controller Value", 0.0, ops.getSpeedController().get(), 0.002);

        // Verify no direction change, from reverse to stop
        ops.setSpeed(-0.5);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#2: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);
        assertEquals("#2: Wrong Speed Controller Value", 0.0, ops.getSpeedController().get(), 0.002);

        //Attempt to change speed from stopped        ops.setSpeed(0.0);
        ops.modifySpeedWithOutDirectionChange(0.7);
        assertEquals("#3: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);
        assertEquals("#3: Wrong Speed Controller Value", 0.0, ops.getSpeedController().get(), 0.002);

        //From forward to increased forward, should be constrained
        ops.setSpeed(0.1);
        ops.modifySpeedWithOutDirectionChange(2.0);
        assertEquals("#4: Wrong MotorOps Speed Setting", 1.0, ops.getSpeed(), 0.002);
        assertEquals("#4: Wrong Speed Controller Value", -1.0, ops.getSpeedController().get(), 0.002);

        //From reverse to increased reverse, should be constrained
        ops.setSpeed(-0.1);
        ops.modifySpeedWithOutDirectionChange(2.0);
        assertEquals("#5: Wrong MotorOps Speed Setting", -1.0, ops.getSpeed(), 0.002);
        assertEquals("#5: Wrong Speed Controller Value", 1.0, ops.getSpeedController().get(), 0.002);

        //Forward speed increases
        ops.setSpeed(0.1);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#6a: Wrong MotorOps Speed Setting", 0.2, ops.getSpeed(), 0.002);
        assertEquals("#6a: Wrong Speed Controller Value", -0.2, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#6b: Wrong MotorOps Speed Setting", 0.3, ops.getSpeed(), 0.002);
        assertEquals("#6b: Wrong Speed Controller Value", -0.3, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.2);
        assertEquals("#6c: Wrong MotorOps Speed Setting", 0.5, ops.getSpeed(), 0.002);
        assertEquals("#6c: Wrong Speed Controller Value", -0.5, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.7);
        assertEquals("#6d: Wrong MotorOps Speed Setting", 1.0, ops.getSpeed(), 0.002);
        assertEquals("#6d: Wrong Speed Controller Value", -1.0, ops.getSpeedController().get(), 0.002);

        //Forward speed decreases
        ops.setSpeed(1.0);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#7a: Wrong MotorOps Speed Setting", 0.9, ops.getSpeed(), 0.002);
        assertEquals("#7a: Wrong Speed Controller Value", -0.9, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#7b: Wrong MotorOps Speed Setting", 0.8, ops.getSpeed(), 0.002);
        assertEquals("#7b: Wrong Speed Controller Value", -0.8, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.2);
        assertEquals("#7c: Wrong MotorOps Speed Setting", 0.6, ops.getSpeed(), 0.002);
        assertEquals("#7c: Wrong Speed Controller Value", -0.6, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#7d: Wrong MotorOps Speed Setting", 0.0, ops.getSpeed(), 0.002);
        assertEquals("#7d: Wrong Speed Controller Value", 0.0, ops.getSpeedController().get(), 0.002);

        //Reverse speed increases
        ops.setSpeed(-0.1);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#8a: Wrong MotorOps Speed Setting", -0.2, ops.getSpeed(), 0.002);
        assertEquals("#8a: Wrong Speed Controller Value", 0.2, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.1);
        assertEquals("#8b: Wrong MotorOps Speed Setting", -0.3, ops.getSpeed(), 0.002);
        assertEquals("#8b: Wrong Speed Controller Value", 0.3, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.2);
        assertEquals("#8c: Wrong MotorOps Speed Setting", -0.5, ops.getSpeed(), 0.002);
        assertEquals("#8c: Wrong Speed Controller Value", 0.5, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(0.7);
        assertEquals("#8d: Wrong MotorOps Speed Setting", -1.0, ops.getSpeed(), 0.002);
        assertEquals("#8d: Wrong Speed Controller Value", 1.0, ops.getSpeedController().get(), 0.002);


        //Reverse speed decreases
        ops.setSpeed(-1.0);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#9a: Wrong MotorOps Speed Setting", -0.9, ops.getSpeed(), 0.002);
        assertEquals("#9a: Wrong Speed Controller Value", 0.9, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.1);
        assertEquals("#9b: Wrong MotorOps Speed Setting", -0.8, ops.getSpeed(), 0.002);
        assertEquals("#9b: Wrong Speed Controller Value", 0.8, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.2);
        assertEquals("#9c: Wrong MotorOps Speed Setting", -0.6, ops.getSpeed(), 0.002);
        assertEquals("#9c: Wrong Speed Controller Value", 0.6, ops.getSpeedController().get(), 0.002);
        ops.modifySpeedWithOutDirectionChange(-0.7);
        assertEquals("#9d: Wrong MotorOps Speed Setting", -0.0, ops.getSpeed(), 0.002);
        assertEquals("#9d: Wrong Speed Controller Value", 0.0, ops.getSpeedController().get(), 0.002);
    }
}